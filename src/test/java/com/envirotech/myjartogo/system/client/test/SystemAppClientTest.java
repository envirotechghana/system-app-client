package com.envirotech.myjartogo.system.client.test;

import static org.junit.Assert.*;
import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsNull.*;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.security.auth.Subject;
import javax.security.auth.login.LoginException;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import com.envirotech.myjartogo.system.client.InventoryClient;
import com.envirotech.myjartogo.system.client.LocationClient;
import com.envirotech.myjartogo.system.client.MealClient;
import com.envirotech.myjartogo.system.client.MealInventoryClient;
import com.envirotech.myjartogo.system.client.OrderClient;
import com.envirotech.myjartogo.system.client.RegistrationClient;
import com.envirotech.myjartogo.system.client.ServiceVendorRegistrationClient;
import com.envirotech.myjartogo.system.client.SystemAppClient;
import com.envirotech.myjartogo.system.client.TicketClient;
import com.envirotech.myjartogo.system.client.UnitClient;
import com.envirotech.myjartogo.system.client.VendorRegistrationClient;
import com.envirotech.myjartogo.system.client.exception.SystemAppError;
import com.envirotech.myjartogo.system.client.model.ActionResponse;
import com.envirotech.myjartogo.system.client.model.ErrorResponse;
import com.envirotech.myjartogo.system.client.model.Inventory;
import com.envirotech.myjartogo.system.client.model.Location;
import com.envirotech.myjartogo.system.client.model.Meal;
import com.envirotech.myjartogo.system.client.model.MealInventory;
import com.envirotech.myjartogo.system.client.model.Order;
import com.envirotech.myjartogo.system.client.model.OrderStatus;
import com.envirotech.myjartogo.system.client.model.Registration;
import com.envirotech.myjartogo.system.client.model.ServiceVendorRegistration;
import com.envirotech.myjartogo.system.client.model.Ticket;
import com.envirotech.myjartogo.system.client.model.Unit;
import com.envirotech.myjartogo.system.client.model.VendorRegistration;
import com.envirotech.myjartogo.system.client.utils.SystemEnvironmentEditor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quakearts.rest.client.exception.HttpClientException;
import com.quakearts.rest.client.exception.HttpClientRuntimeException;
import com.quakearts.tools.test.mockserver.MockServer;
import com.quakearts.tools.test.mockserver.MockServerFactory;
import com.quakearts.tools.test.mockserver.configuration.Configuration.MockingMode;
import com.quakearts.tools.test.mockserver.configuration.impl.ConfigurationBuilder;
import com.quakearts.tools.test.mockserver.fi.HttpRequestMatcher;
import com.quakearts.webapp.security.auth.JWTLoginModule;
import com.quakearts.webapp.security.auth.JWTPrincipal;
import com.quakearts.webapp.security.auth.UserPrincipal;
import com.quakearts.webapp.security.jwt.JWTClaims;
import com.quakearts.webapp.security.jwt.JWTHeader;
import com.quakearts.webapp.security.jwt.JWTSigner;
import com.quakearts.webapp.security.jwt.exception.JWTException;
import com.quakearts.webapp.security.jwt.factory.JWTFactory;

import static com.quakearts.tools.test.mockserver.model.impl.MockActionBuilder.*;
import static com.quakearts.tools.test.mockserver.model.impl.HttpMessageBuilder.*;
import com.quakearts.webtools.test.CDIRunner;

import junit.framework.AssertionFailedError;

@RunWith(CDIRunner.class)
public class SystemAppClientTest {

	private static MockServer mockServer;
		
	private static Unit unit;
	private static Inventory inventory;
	private static Location location;
	private static Meal meal;
	private static MealInventory mealInventory;
	private static Registration registration;
	private static Order order;
	private static ServiceVendorRegistration registration2;
	private static Ticket ticket;
	private static VendorRegistration registration3;
	private static boolean firstToken;
	private static boolean generatedNew;
	
	@Inject
	private InventoryClient inventoryClient;
	
	@BeforeClass
	public static void initTestServer() throws Exception {

		SystemEnvironmentEditor.instance().set("JARTOGO_AUTHENTICATION__URL")
		.as("http://localhost:8083");
		
		HttpRequestMatcher bodyMatcher = (request, incoming)->{
			if(!request.getResource().equals(incoming.getResource())
					|| !request.getMethod().equals(incoming.getMethod())){
				return false;
			}
			
			return Arrays.equals(request.getContentBytes(), incoming.getContentBytes());
		};

		unit = new Unit();
		unit.setName("unit");
		unit.setSymbol("symbol");
		
		inventory = new Inventory();
		inventory.setCreateDate(LocalDateTime.now());
		inventory.setId(1);
		inventory.setNotes("Test Note");
		inventory.setProductId("Product1");
		inventory.setQuantity(new BigDecimal(5));
		inventory.setUnit(unit);
		inventory.setUnitPrice(new BigDecimal("2.00"));
		inventory.setUnitSalePrice(new BigDecimal("1.00"));
		
		location = new Location();
		location.setAddressLine1("address1");
		location.setAddressLine2("address2");
		location.setCountry("country");
		location.setId(2);
		location.setLocationName("location");
		location.setPostalCode("postalCode");
		
		meal = new Meal();
		meal.setBasePrice(new BigDecimal("3.00"));
		meal.setCreateDate(LocalDateTime.now());
		meal.setDescription("description");
		meal.setId("meal1");

		registration = new Registration();
		registration.setCreateDate(LocalDate.now());
		registration.setDeleted(true);
		registration.setDeviceId("deviceId1");
		registration.setFullName("name");
		registration.setMobileNo("0201234567");
		registration.setPin("pin");
		
		order = new Order();
		order.setCreateDate(LocalDateTime.now());
		order.setId(3);
		order.setInventoryRecord(inventory);
		order.setQuantity(new BigDecimal(6));
		order.setRequestor(registration);
		order.setStatus(OrderStatus.CREATED);
		order.setUnit(unit);
		
		registration2 = new ServiceVendorRegistration();
		registration2.setCreateDate(LocalDate.now());
		registration2.setDeleted(true);
		registration2.setDeviceId("deviceId2");
		registration2.setFullName("name2");
		registration2.setMobileNo("0201234568");
		registration2.setPin("pin2");
		registration2.setCurrentlyActive(true);
		registration2.setServiceLocation(location);
		
		ticket = new Ticket();
		ticket.setActive(true);
		ticket.setCreateDate(LocalDateTime.now());
		ticket.setId(4);
		ticket.setRegistration(registration);
		ticket.setTypeCode("ticket");
		ticket.setValue("value1");
		
		registration3 = new VendorRegistration();
		registration3.setCreateDate(LocalDate.now());
		registration3.setDeleted(true);
		registration3.setDeviceId("deviceId3");
		registration3.setFullName("name3");
		registration3.setMobileNo("0201234569");
		registration3.setPin("pin3");
		registration3.setCentralKitchenVendor(true);
		registration3.setVendorLocation(location);		
		
		mealInventory = new MealInventory();
		mealInventory.setCreateDate(LocalDateTime.now());
		mealInventory.setId(1);
		mealInventory.setNotes("Test Note2");
		mealInventory.setProductId("Product2");
		mealInventory.setQuantity(new BigDecimal(7));
		mealInventory.setUnit(unit);
		mealInventory.setUnitPrice(new BigDecimal("5.00"));
		mealInventory.setUnitSalePrice(new BigDecimal("10.00"));		
		mealInventory.setMeal(meal);
		mealInventory.setServiceVendor(registration2);
		mealInventory.setServiceVendorUnitSalePrice(new BigDecimal("3.00"));
		mealInventory.setVendor(registration3);
		
		ErrorResponse errorResponse = new ErrorResponse()
				.addExplanation("Test Error 1")
				.addExplanation("Test Error 2");
		ObjectMapper objectMapper = CDI.current().select(ObjectMapper.class).get();
		
		mockServer = MockServerFactory.getInstance().getMockServer()
				.configure(ConfigurationBuilder
						.newConfiguration().setPortAs(8083).setMockingModeAs(MockingMode.MOCK).thenBuild())
				.add(createNewMockAction()
						.setRequestAs(createNewHttpRequest()
						.setId("GET-Authenticate-User")
						.setResourceAs("/authenticate/system-app/My-Jartogo-System?clientId=testuser&credential=password1")
						.setMethodAs("GET")
						.setResponseAs(createNewHttpResponse()
								.setResponseCodeAs(200)
								.setContentBytes(createExpiredToken())
								.thenBuild())
						.thenBuild())
						.setResponseActionAs((request, response)->{
							if(firstToken){
								generatedNew = true;
								return createNewHttpResponse()
										.setResponseCodeAs(200)
										.setContentBytes(createToken())
										.thenBuild();
							} else {
								firstToken = true;
								return response;
							}
						})
					.thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-Inventory-Error-ID")
							.setMethodAs("GET").setResourceAs("/inventory/5")
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(404)
									.setContentBytes(objectMapper.writeValueAsBytes(errorResponse))
								.thenBuild())
							.thenBuild())
						.thenBuild(),
						createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-Inventory-Error-ID")
								.setMethodAs("GET").setResourceAs("/inventory/6")
								.setResponseAs(createNewHttpResponse().setResponseCodeAs(200)
										.setContentBytes("<invalid /><response />".getBytes())
									.thenBuild())
								.thenBuild())
							.thenBuild(),
						createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-Inventory-Error-ID")
								.setMethodAs("GET").setResourceAs("/inventory/7")
								.setResponseAs(createNewHttpResponse().setResponseCodeAs(404)
										.setContentBytes("<invalid /><error />".getBytes())
									.thenBuild())
								.thenBuild())
							.thenBuild()
						);
			

		for (Object object : new Object[] { unit, inventory, location, meal, 
				mealInventory, registration, order,
				registration2, ticket, registration3 }) {
			String resourceName = process(object.getClass());
			String resource = "/"+resourceName;
			byte[] body = objectMapper.writeValueAsBytes(object);
			byte[] bodyList = objectMapper.writeValueAsBytes(Arrays.asList(object));
			
			mockServer.add(
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-"+resourceName+"-ID")
							.setMethodAs("GET").setResourceAs(resource+"/1")
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(200)
									.setContentBytes(body).thenBuild())
							.thenBuild()).thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-"+resourceName+"-With-Filter-ID")
							.setMethodAs("GET").setResourceAs(resource+"/1?return=test1")
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(200)
									.setContentBytes(body).thenBuild())
							.thenBuild()).thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-"+resourceName+"-With-Filters-ID")
							.setMethodAs("GET").setResourceAs(resource+"/1?return=test1,test2,test3")
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(200)
									.setContentBytes(body).thenBuild())
							.thenBuild())
						.setMatcherAs(urlMatcher())
						.thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-"+resourceName+"-Find-No-Param-Or-Filter")
							.setMethodAs("GET").setResourceAs(resource)
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(200)
									.setContentBytes(bodyList)
									.thenBuild())
							.thenBuild()).thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-"+resourceName+"-Find-Param-And-Filter")
							.setMethodAs("GET").setResourceAs(resource+"?value=test2&return=test1")
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(200)
									.setContentBytes("[]".getBytes()).thenBuild())
							.thenBuild())
						.setMatcherAs(urlMatcher())
						.thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-"+resourceName+"-Find-Params-And-Filters")
							.setMethodAs("GET").setResourceAs(resource+"?value=test2&id=3&return=test1,test2")
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(200)
									.setContentBytes("[]".getBytes()).thenBuild())
							.thenBuild())
							.setMatcherAs(urlMatcher())
							.thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-"+resourceName+"-Find-Params-And-Filters")
							.setMethodAs("GET").setResourceAs(resource+"?value=test2&id=3")
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(200)
									.setContentBytes("[]".getBytes()).thenBuild())
							.thenBuild())
							.setMatcherAs(urlMatcher())
							.thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-"+resourceName+"-Find-Params-And-Filters")
							.setMethodAs("GET").setResourceAs(resource+"?return=test1,test2")
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(200)
									.setContentBytes("[]".getBytes()).thenBuild())
							.thenBuild())
							.thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-"+resourceName+"-Find-Params-And-Filters")
							.setMethodAs("GET").setResourceAs(resource+"?value=test2")
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(200)
									.setContentBytes(bodyList)
									.thenBuild())
							.thenBuild())
							.thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("GET-"+resourceName+"-Find-Params-And-Filters")
							.setMethodAs("GET").setResourceAs(resource+"?return=test1")
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(200)
									.setContentBytes(bodyList)
									.thenBuild())
							.thenBuild())
							.thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("POST-"+resourceName)
							.setMethodAs("POST").setResourceAs(resource)
							.setContentBytes(body)
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(204).thenBuild()).thenBuild())
							.setMatcherAs(bodyMatcher).thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("PUT-"+resourceName)
							.setMethodAs("PUT").setResourceAs(resource+"/2")
							.setContentBytes(body)
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(204).thenBuild()).thenBuild())
							.setMatcherAs(bodyMatcher).thenBuild(),
					createNewMockAction().setRequestAs(createNewHttpRequest().setId("DELETE-"+resourceName)
							.setMethodAs("DELETE").setResourceAs(resource+"/3")
							.setResponseAs(createNewHttpResponse().setResponseCodeAs(204).thenBuild()).thenBuild())
							.thenBuild());
		}
		
		mockServer.start();
	}

	private static HttpRequestMatcher urlMatcher() {
		return (request, incoming)->{
			try {
				URL requestUrl = new URL("http://test.com"+request.getResource());
				URL incomeingUrl = new URL("http://test.com"+incoming.getResource());
				return request.getMethod().equals(incoming.getMethod())
						&& requestUrl.getPath().equals(incomeingUrl.getPath())
						&& isEqual(requestUrl.getQuery(), incomeingUrl.getQuery());
			} catch (MalformedURLException e) {
				throw new AssertionFailedError(e.getMessage());
			}
		};
	}

	private static boolean isEqual(String query, String query2) {
		return queryToMap(query).equals(queryToMap(query2));
	}

	protected static Map<String, String> queryToMap(String query) {
		return query !=null? Arrays.asList(query.split("&"))
				.stream().map(queryPart->queryPart.split("="))
				.collect(Collectors.toMap(partArray->partArray.length>0?partArray[0]:"", 
						partArray->partArray.length>1?partArray[1]:""))
				: Collections.emptyMap();
	}

	private static String process(Class<?> targetClass) {
		return targetClass.getSimpleName().chars().mapToObj(c->Character.isUpperCase(c)?
				"-"+((char)Character.toLowerCase(c)): Character.toString((char)c)
		).collect(Collectors.joining()).substring(1);
	}
	
	private static byte[] createExpiredToken() throws JWTException {
		JWTFactory factory = JWTFactory.getInstance();
		JWTHeader header = factory.createEmptyClaimsHeader();
		JWTClaims claims = factory.createEmptyClaims();
		claims.setAudience("http://test.com");
		claims.setIssuedAt((System.currentTimeMillis()-67000)/1000);
		claims.setIssuer("http://test.com");
		claims.setExpiry((System.currentTimeMillis()-6000)/1000);
		claims.setNotBefore((System.currentTimeMillis()-66000)/1000);
		claims.setSubject("test");
		
		Map<String, String> options = new HashMap<>();
		options.put("secret", "SECRET");
		JWTSigner jwtSigner = factory.getSigner("HS256", options);
		return ("{\r\n" + 
				"    \"tokenType\": \"bearer\",\r\n" + 
				"    \"expiresIn\": 300,\r\n" + 
				"    \"idToken\": \""+ jwtSigner.sign(header, claims)
				+"\"\r\n" + 
				"}").getBytes();
	}

	private static byte[] createToken() {
		Subject subject = new Subject();
		HashMap<String, String> options = new HashMap<>();
		options.put("algorithm", "HS256");
		options.put("issuer", "https://authenticator.envirotech.com");
		options.put("audience", "https://myjartogo.envirotech.com");
		options.put("secret", "SECRET");
		options.put("validity.period", "1 Second");
		options.put("additional.claims", "Read;Write;");
		HashMap<String, Object> state = new HashMap<>();
		state.put("javax.security.auth.login.name", new UserPrincipal("testuser"));
		state.put("com.quakearts.LoginOk", Boolean.TRUE);
		
		JWTLoginModule jwtLoginModule = new JWTLoginModule();
		jwtLoginModule.initialize(subject, callbacks->{}, state, options);
		try {
			jwtLoginModule.login();
			jwtLoginModule.commit();
		} catch (LoginException e) {
			throw new AssertionFailedError(e.getMessage());
		}
		
		JWTPrincipal jwtPrincipal = subject.getPrincipals(JWTPrincipal.class)
				.iterator().next();
		
		
		return ("{\r\n" + 
				"    \"tokenType\": \"bearer\",\r\n" + 
				"    \"expiresIn\": 300,\r\n" + 
				"    \"idToken\": \""+jwtPrincipal.getName()
				+"\"\r\n" + 
				"}").getBytes();
	}
	
	@AfterClass
	public static void stopTestServer() {
		mockServer.stop();
	}

	@Inject
	private TestSystemErrorApp testSystemApp;
	
	@Test
	public void testInventoryClient() throws Exception {
		targetClass = Inventory.class;
		assertThat(new TestSystemApp().getRootResource(), is("/inventory"));

		Inventory inventoryGet = inventoryClient.get(1l);
		assertThat(inventoryGet.getCreateDate(), is(inventory.getCreateDate()));
		assertThat(inventoryGet.getId(), is(inventory.getId()));
		assertThat(inventoryGet.getNotes(), is(inventory.getNotes()));
		assertThat(inventoryGet.getProductId(), is(inventory.getProductId()));
		assertThat(inventoryGet.getQuantity(), is(inventory.getQuantity()));
		assertThat(inventoryGet.getUnit(), is(notNullValue()));
		assertThat(inventoryGet.getUnit().getName(), is(inventory.getUnit().getName()));
		assertThat(inventoryGet.getUnit().getSymbol(), is(inventory.getUnit().getSymbol()));
		assertThat(inventoryGet.getUnitPrice(), is(inventory.getUnitPrice()));
		assertThat(inventoryGet.getUnitSalePrice(), is(inventory.getUnitSalePrice()));
		
		inventoryGet = inventoryClient.get(1l, Arrays.asList("test1"));
		assertThat(inventoryGet.getCreateDate(), is(inventory.getCreateDate()));
		assertThat(inventoryGet.getId(), is(inventory.getId()));
		assertThat(inventoryGet.getNotes(), is(inventory.getNotes()));
		assertThat(inventoryGet.getProductId(), is(inventory.getProductId()));
		assertThat(inventoryGet.getQuantity(), is(inventory.getQuantity()));
		assertThat(inventoryGet.getUnit(), is(notNullValue()));
		assertThat(inventoryGet.getUnit().getName(), is(inventory.getUnit().getName()));
		assertThat(inventoryGet.getUnit().getSymbol(), is(inventory.getUnit().getSymbol()));
		assertThat(inventoryGet.getUnitPrice(), is(inventory.getUnitPrice()));
		assertThat(inventoryGet.getUnitSalePrice(), is(inventory.getUnitSalePrice()));
		
		inventoryGet = inventoryClient.get(1l, Arrays.asList("test1","test2","test3"));
		assertThat(inventoryGet.getCreateDate(), is(inventory.getCreateDate()));
		assertThat(inventoryGet.getId(), is(inventory.getId()));
		assertThat(inventoryGet.getNotes(), is(inventory.getNotes()));
		assertThat(inventoryGet.getProductId(), is(inventory.getProductId()));
		assertThat(inventoryGet.getQuantity(), is(inventory.getQuantity()));
		assertThat(inventoryGet.getUnit(), is(notNullValue()));
		assertThat(inventoryGet.getUnit().getName(), is(inventory.getUnit().getName()));
		assertThat(inventoryGet.getUnit().getSymbol(), is(inventory.getUnit().getSymbol()));
		assertThat(inventoryGet.getUnitPrice(), is(inventory.getUnitPrice()));
		assertThat(inventoryGet.getUnitSalePrice(), is(inventory.getUnitSalePrice()));
		
		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("value", "test2");
		List<Inventory> inventories = inventoryClient.find(parameters, Collections.emptyList());
		assertThat(inventories.size(), is(1));
		inventoryGet = inventories.get(0);
		assertThat(inventoryGet.getCreateDate(), is(inventory.getCreateDate()));
		assertThat(inventoryGet.getId(), is(inventory.getId()));
		assertThat(inventoryGet.getNotes(), is(inventory.getNotes()));
		assertThat(inventoryGet.getProductId(), is(inventory.getProductId()));
		assertThat(inventoryGet.getQuantity(), is(inventory.getQuantity()));
		assertThat(inventoryGet.getUnit(), is(notNullValue()));
		assertThat(inventoryGet.getUnit().getName(), is(inventory.getUnit().getName()));
		assertThat(inventoryGet.getUnit().getSymbol(), is(inventory.getUnit().getSymbol()));
		assertThat(inventoryGet.getUnitPrice(), is(inventory.getUnitPrice()));
		assertThat(inventoryGet.getUnitSalePrice(), is(inventory.getUnitSalePrice()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		inventories = inventoryClient.find(parameters, Collections.emptyList());
		assertThat(inventories.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		inventories = inventoryClient.find(parameters, Arrays.asList("test1","test2"));
		assertThat(inventories.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		inventories = inventoryClient.find(parameters, Arrays.asList("test1"));
		assertThat(inventories.isEmpty(), is(true));

		inventories = inventoryClient.find(Collections.emptyMap(), Arrays.asList("test1","test2"));
		assertThat(inventories.isEmpty(), is(true));
		
		inventories = inventoryClient.find(Collections.emptyMap(), Collections.emptyList());
		assertThat(inventories.size(), is(1));
		inventoryGet = inventories.get(0);
		assertThat(inventoryGet.getCreateDate(), is(inventory.getCreateDate()));
		assertThat(inventoryGet.getId(), is(inventory.getId()));
		assertThat(inventoryGet.getNotes(), is(inventory.getNotes()));
		assertThat(inventoryGet.getProductId(), is(inventory.getProductId()));
		assertThat(inventoryGet.getQuantity(), is(inventory.getQuantity()));
		assertThat(inventoryGet.getUnit(), is(notNullValue()));
		assertThat(inventoryGet.getUnit().getName(), is(inventory.getUnit().getName()));
		assertThat(inventoryGet.getUnit().getSymbol(), is(inventory.getUnit().getSymbol()));
		assertThat(inventoryGet.getUnitPrice(), is(inventory.getUnitPrice()));
		assertThat(inventoryGet.getUnitSalePrice(), is(inventory.getUnitSalePrice()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		inventories = inventoryClient.find(parameters, Collections.emptyList());
		assertThat(inventories.size(), is(1));
		inventoryGet = inventories.get(0);
		assertThat(inventoryGet.getCreateDate(), is(inventory.getCreateDate()));
		assertThat(inventoryGet.getId(), is(inventory.getId()));
		assertThat(inventoryGet.getNotes(), is(inventory.getNotes()));
		assertThat(inventoryGet.getProductId(), is(inventory.getProductId()));
		assertThat(inventoryGet.getQuantity(), is(inventory.getQuantity()));
		assertThat(inventoryGet.getUnit(), is(notNullValue()));
		assertThat(inventoryGet.getUnit().getName(), is(inventory.getUnit().getName()));
		assertThat(inventoryGet.getUnit().getSymbol(), is(inventory.getUnit().getSymbol()));
		assertThat(inventoryGet.getUnitPrice(), is(inventory.getUnitPrice()));
		assertThat(inventoryGet.getUnitSalePrice(), is(inventory.getUnitSalePrice()));
		
		inventories = inventoryClient.find(Collections.emptyMap(), Arrays.asList("test1"));
		assertThat(inventories.size(), is(1));
		inventoryGet = inventories.get(0);
		assertThat(inventoryGet.getCreateDate(), is(inventory.getCreateDate()));
		assertThat(inventoryGet.getId(), is(inventory.getId()));
		assertThat(inventoryGet.getNotes(), is(inventory.getNotes()));
		assertThat(inventoryGet.getProductId(), is(inventory.getProductId()));
		assertThat(inventoryGet.getQuantity(), is(inventory.getQuantity()));
		assertThat(inventoryGet.getUnit(), is(notNullValue()));
		assertThat(inventoryGet.getUnit().getName(), is(inventory.getUnit().getName()));
		assertThat(inventoryGet.getUnit().getSymbol(), is(inventory.getUnit().getSymbol()));
		assertThat(inventoryGet.getUnitPrice(), is(inventory.getUnitPrice()));
		assertThat(inventoryGet.getUnitSalePrice(), is(inventory.getUnitSalePrice()));
		
		ActionResponse actionResponse = inventoryClient.create(inventory);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = inventoryClient.update(inventory, 2L);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = inventoryClient.delete(3L);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		assertThat(generatedNew, is(true));
	}
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void testResponseError() throws Exception {
		expectedException.expect(SystemAppError.class);
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setExplanations(Arrays.asList("Test Error 1","Test Error 2"));
		SystemAppError error = new SystemAppError(errorResponse, 404);
		expectedException.expect(isException(error));
		inventoryClient.get(5l);
	}
	
	@Test
	public void testRequestMarshallingError() throws Exception {
		expectedException.expect(HttpClientException.class);
		expectedException.expectMessage("Unable to marshal request object");
		inventoryClient.create(new InventoryBomb());
	}
	
	@Test
	public void testResponseMarshallingError() throws Exception {
		expectedException.expect(SystemAppError.class);
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setExplanations(Arrays.asList("<invalid /><response />"));
		SystemAppError error = new SystemAppError(errorResponse, 200);
		expectedException.expect(isException(error));
		inventoryClient.get(6l);
	}
	
	@Test
	public void testResponseErrorMarshallingError() throws Exception {
		expectedException.expect(SystemAppError.class);
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setExplanations(Arrays.asList("<invalid /><error />"));
		SystemAppError error = new SystemAppError(errorResponse, 404);
		expectedException.expect(isException(error));
		inventoryClient.get(7l);
	}
	
	@Test
	public void testTestURLEncodeError() throws Exception {
		expectedException.expect(HttpClientRuntimeException.class);
		expectedException.expectMessage("Encoding error");
		targetClass = Inventory.class;
		testSystemApp.configureEncodeError();
		Map<String, String> parameters = new HashMap<>();
		parameters.put("test", "value");
		testSystemApp.find(parameters,Collections.emptyList());
	}
	
	@Test
	public void testInvalidGetIDError() throws Exception {
		targetClass = Inventory.class;
		expectedException.expect(HttpClientRuntimeException.class);
		expectedException.expectMessage("Invalid ID type. Must be a "+inventory.getClass().getSimpleName());
		new TestSystemApp().get("");
	}
	
	@Test
	public void testInvalidDeleteIDError() throws Exception {
		targetClass = Inventory.class;
		expectedException.expect(HttpClientRuntimeException.class);
		expectedException.expectMessage("Invalid ID type. Must be a "+inventory.getClass().getSimpleName());
		new TestSystemApp().delete("");
	}
	
	@Test
	public void testInvalidUpdateIDError() throws Exception {
		targetClass = Inventory.class;
		expectedException.expect(HttpClientRuntimeException.class);
		expectedException.expectMessage("Invalid ID type. Must be a "+inventory.getClass().getSimpleName());
		new TestSystemApp().update(new Inventory(), "");
	}

	
	private Matcher<Throwable> isException(SystemAppError error){
		return new BaseMatcher<Throwable>() {
			@Override
			public void describeTo(Description description) {
				description.appendText("SystemAppError error with message ")
				.appendText(error.getMessage()).appendText(" and error response code ")
				.appendText(" and explanations ")
				.appendText(error.getErrorResponse()!=null
				&& error.getErrorResponse().getExplanations()!=null
				?error.getErrorResponse().getExplanations().toString():"");
			}
			
			@Override
			public boolean matches(Object item) {
				if(item instanceof SystemAppError){
					SystemAppError inError = (SystemAppError) item;
					
					if(error.getHttpCode() != inError.getHttpCode())
						return false;
					
					if(error.getErrorResponse() == null)
						return error.getErrorResponse() == inError.getErrorResponse();
					
					if(error.getErrorResponse().getExplanations() == null)
						return error.getErrorResponse().getExplanations() == inError.getErrorResponse().getExplanations();
					
					return error.getErrorResponse().getExplanations().equals(inError.getErrorResponse().getExplanations());
				}
				return false;
			}
		};
	}
	
	@Inject
	private LocationClient locationClient;
	
	@Test
	public void testLocationClient() throws Exception {
		targetClass = Location.class;
		
		assertThat(new TestSystemApp().getRootResource(), is("/location"));
		Location locationGet = locationClient.get(1l);
		assertThat(locationGet.getAddressLine1(),is(location.getAddressLine1()));
		assertThat(locationGet.getAddressLine2(),is(location.getAddressLine2()));
		assertThat(locationGet.getCountry(),is(location.getCountry()));
		assertThat(locationGet.getId(),is(location.getId()));
		assertThat(locationGet.getLocationName(),is(location.getLocationName()));
		assertThat(locationGet.getPostalCode(),is(location.getPostalCode()));

		locationGet = locationClient.get(1l, Arrays.asList("test1"));
		assertThat(locationGet.getAddressLine1(),is(location.getAddressLine1()));
		assertThat(locationGet.getAddressLine2(),is(location.getAddressLine2()));
		assertThat(locationGet.getCountry(),is(location.getCountry()));
		assertThat(locationGet.getId(),is(location.getId()));
		assertThat(locationGet.getLocationName(),is(location.getLocationName()));
		assertThat(locationGet.getPostalCode(),is(location.getPostalCode()));

		locationGet = locationClient.get(1l, Arrays.asList("test1","test2","test3"));
		assertThat(locationGet.getAddressLine1(),is(location.getAddressLine1()));
		assertThat(locationGet.getAddressLine2(),is(location.getAddressLine2()));
		assertThat(locationGet.getCountry(),is(location.getCountry()));
		assertThat(locationGet.getId(),is(location.getId()));
		assertThat(locationGet.getLocationName(),is(location.getLocationName()));
		assertThat(locationGet.getPostalCode(),is(location.getPostalCode()));

		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("value", "test2");
		List<Location> locations = locationClient.find(parameters, Collections.emptyList());
		assertThat(locations.size(), is(1));
		locationGet = locations.get(0);
		assertThat(locationGet.getAddressLine1(),is(location.getAddressLine1()));
		assertThat(locationGet.getAddressLine2(),is(location.getAddressLine2()));
		assertThat(locationGet.getCountry(),is(location.getCountry()));
		assertThat(locationGet.getId(),is(location.getId()));
		assertThat(locationGet.getLocationName(),is(location.getLocationName()));
		assertThat(locationGet.getPostalCode(),is(location.getPostalCode()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		locations = locationClient.find(parameters, Collections.emptyList());
		assertThat(locations.isEmpty(), is(true));
		
		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		locations = locationClient.find(parameters, Arrays.asList("test1","test2"));
		assertThat(locations.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		locations = locationClient.find(parameters, Arrays.asList("test1"));
		assertThat(locations.isEmpty(), is(true));

		locations = locationClient.find(Collections.emptyMap(), Arrays.asList("test1","test2"));
		assertThat(locations.isEmpty(), is(true));
		
		locations = locationClient.find(Collections.emptyMap(), Collections.emptyList());
		assertThat(locations.size(), is(1));
		locationGet = locations.get(0);
		assertThat(locationGet.getAddressLine1(),is(location.getAddressLine1()));
		assertThat(locationGet.getAddressLine2(),is(location.getAddressLine2()));
		assertThat(locationGet.getCountry(),is(location.getCountry()));
		assertThat(locationGet.getId(),is(location.getId()));
		assertThat(locationGet.getLocationName(),is(location.getLocationName()));
		assertThat(locationGet.getPostalCode(),is(location.getPostalCode()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		locations = locationClient.find(parameters, Collections.emptyList());
		assertThat(locations.size(), is(1));
		locationGet = locations.get(0);
		assertThat(locationGet.getAddressLine1(),is(location.getAddressLine1()));
		assertThat(locationGet.getAddressLine2(),is(location.getAddressLine2()));
		assertThat(locationGet.getCountry(),is(location.getCountry()));
		assertThat(locationGet.getId(),is(location.getId()));
		assertThat(locationGet.getLocationName(),is(location.getLocationName()));
		assertThat(locationGet.getPostalCode(),is(location.getPostalCode()));
		
		locations = locationClient.find(Collections.emptyMap(), Arrays.asList("test1"));
		assertThat(locations.size(), is(1));
		locationGet = locations.get(0);
		assertThat(locationGet.getAddressLine1(),is(location.getAddressLine1()));
		assertThat(locationGet.getAddressLine2(),is(location.getAddressLine2()));
		assertThat(locationGet.getCountry(),is(location.getCountry()));
		assertThat(locationGet.getId(),is(location.getId()));
		assertThat(locationGet.getLocationName(),is(location.getLocationName()));
		assertThat(locationGet.getPostalCode(),is(location.getPostalCode()));

		ActionResponse actionResponse = locationClient.create(location);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = locationClient.update(location, 2L);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = locationClient.delete(3L);
		assertThat(actionResponse.getHttpCode(), is(204));
	}
	
	@Inject
	private MealClient mealClient;
	
	@Test
	public void testMealClient() throws Exception {
		targetClass = Meal.class;
		
		assertThat(new TestSystemApp().getRootResource(), is("/meal"));
		
		Meal mealGet = mealClient.get(1L);
		assertThat(mealGet.getBasePrice(),is(meal.getBasePrice()));
		assertThat(mealGet.getCreateDate(),is(meal.getCreateDate()));
		assertThat(mealGet.getDescription(),is(meal.getDescription()));
		assertThat(mealGet.getId(),is(meal.getId()));

		mealGet = mealClient.get(1L, Arrays.asList("test1"));
		assertThat(mealGet.getBasePrice(),is(meal.getBasePrice()));
		assertThat(mealGet.getCreateDate(),is(meal.getCreateDate()));
		assertThat(mealGet.getDescription(),is(meal.getDescription()));
		assertThat(mealGet.getId(),is(meal.getId()));

		mealGet = mealClient.get(1L, Arrays.asList("test1","test2","test3"));
		assertThat(mealGet.getBasePrice(),is(meal.getBasePrice()));
		assertThat(mealGet.getCreateDate(),is(meal.getCreateDate()));
		assertThat(mealGet.getDescription(),is(meal.getDescription()));
		assertThat(mealGet.getId(),is(meal.getId()));
		
		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("value", "test2");
		List<Meal> meals = mealClient.find(parameters, Collections.emptyList());
		assertThat(meals.size(), is(1));
		mealGet = meals.get(0);
		assertThat(mealGet.getBasePrice(),is(meal.getBasePrice()));
		assertThat(mealGet.getCreateDate(),is(meal.getCreateDate()));
		assertThat(mealGet.getDescription(),is(meal.getDescription()));
		assertThat(mealGet.getId(),is(meal.getId()));
		
		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		meals = mealClient.find(parameters, Collections.emptyList());
		assertThat(meals.isEmpty(), is(true));
		
		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		meals = mealClient.find(parameters, Arrays.asList("test1","test2"));
		assertThat(meals.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		meals = mealClient.find(parameters, Arrays.asList("test1"));
		assertThat(meals.isEmpty(), is(true));

		meals = mealClient.find(Collections.emptyMap(), Arrays.asList("test1","test2"));
		assertThat(meals.isEmpty(), is(true));
		
		meals = mealClient.find(Collections.emptyMap(), Collections.emptyList());
		assertThat(meals.size(), is(1));
		mealGet = meals.get(0);
		assertThat(mealGet.getBasePrice(),is(meal.getBasePrice()));
		assertThat(mealGet.getCreateDate(),is(meal.getCreateDate()));
		assertThat(mealGet.getDescription(),is(meal.getDescription()));
		assertThat(mealGet.getId(),is(meal.getId()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		meals = mealClient.find(parameters, Collections.emptyList());
		assertThat(meals.size(), is(1));
		mealGet = meals.get(0);
		assertThat(mealGet.getBasePrice(),is(meal.getBasePrice()));
		assertThat(mealGet.getCreateDate(),is(meal.getCreateDate()));
		assertThat(mealGet.getDescription(),is(meal.getDescription()));
		assertThat(mealGet.getId(),is(meal.getId()));
		
		meals = mealClient.find(Collections.emptyMap(), Arrays.asList("test1"));
		assertThat(meals.size(), is(1));
		mealGet = meals.get(0);
		assertThat(mealGet.getBasePrice(),is(meal.getBasePrice()));
		assertThat(mealGet.getCreateDate(),is(meal.getCreateDate()));
		assertThat(mealGet.getDescription(),is(meal.getDescription()));
		assertThat(mealGet.getId(),is(meal.getId()));

		ActionResponse actionResponse = mealClient.create(meal);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = mealClient.update(meal, 2L);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = mealClient.delete(3L);
		assertThat(actionResponse.getHttpCode(), is(204));
	}
	
	@Inject
	private MealInventoryClient mealInventoryClient;
	
	@Test
	public void testMealInventoryClient() throws Exception {
		targetClass = Meal.class;
		
		assertThat(new TestSystemApp().getRootResource(), is("/meal"));
		
		MealInventory mealInventoryGet = mealInventoryClient.get(1L);
		assertThat(mealInventoryGet.getCreateDate(),is(mealInventory.getCreateDate()));
		assertThat(mealInventoryGet.getId(),is(mealInventory.getId()));
		assertThat(mealInventoryGet.getNotes(),is(mealInventory.getNotes()));
		assertThat(mealInventoryGet.getProductId(),is(mealInventory.getProductId()));
		assertThat(mealInventoryGet.getQuantity(),is(mealInventory.getQuantity()));
		assertThat(mealInventoryGet.getUnit(), is(notNullValue()));
		assertThat(mealInventoryGet.getUnit().getName(),is(mealInventory.getUnit().getName()));
		assertThat(mealInventoryGet.getUnitPrice(),is(mealInventory.getUnitPrice()));
		assertThat(mealInventoryGet.getUnitSalePrice(),is(mealInventory.getUnitSalePrice()));
		assertThat(mealInventoryGet.getMeal(), is(notNullValue()));
		assertThat(mealInventoryGet.getMeal().getId(),is(mealInventory.getMeal().getId()));
		assertThat(mealInventoryGet.getServiceVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getServiceVendor().getMobileNo(),is(mealInventory.getServiceVendor().getMobileNo()));
		assertThat(mealInventoryGet.getServiceVendorUnitSalePrice(),is(mealInventory.getServiceVendorUnitSalePrice()));
		assertThat(mealInventoryGet.getVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getVendor().getMobileNo(),is(mealInventory.getVendor().getMobileNo()));

		mealInventoryGet = mealInventoryClient.get(1L, Arrays.asList("test1"));
		assertThat(mealInventoryGet.getCreateDate(),is(mealInventory.getCreateDate()));
		assertThat(mealInventoryGet.getId(),is(mealInventory.getId()));
		assertThat(mealInventoryGet.getNotes(),is(mealInventory.getNotes()));
		assertThat(mealInventoryGet.getProductId(),is(mealInventory.getProductId()));
		assertThat(mealInventoryGet.getQuantity(),is(mealInventory.getQuantity()));
		assertThat(mealInventoryGet.getUnit(), is(notNullValue()));
		assertThat(mealInventoryGet.getUnit().getName(),is(mealInventory.getUnit().getName()));
		assertThat(mealInventoryGet.getUnitPrice(),is(mealInventory.getUnitPrice()));
		assertThat(mealInventoryGet.getUnitSalePrice(),is(mealInventory.getUnitSalePrice()));
		assertThat(mealInventoryGet.getMeal(), is(notNullValue()));
		assertThat(mealInventoryGet.getMeal().getId(),is(mealInventory.getMeal().getId()));
		assertThat(mealInventoryGet.getServiceVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getServiceVendor().getMobileNo(),is(mealInventory.getServiceVendor().getMobileNo()));
		assertThat(mealInventoryGet.getServiceVendorUnitSalePrice(),is(mealInventory.getServiceVendorUnitSalePrice()));
		assertThat(mealInventoryGet.getVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getVendor().getMobileNo(),is(mealInventory.getVendor().getMobileNo()));
		
		mealInventoryGet = mealInventoryClient.get(1L, Arrays.asList("test1","test2","test3"));
		assertThat(mealInventoryGet.getCreateDate(),is(mealInventory.getCreateDate()));
		assertThat(mealInventoryGet.getId(),is(mealInventory.getId()));
		assertThat(mealInventoryGet.getNotes(),is(mealInventory.getNotes()));
		assertThat(mealInventoryGet.getProductId(),is(mealInventory.getProductId()));
		assertThat(mealInventoryGet.getQuantity(),is(mealInventory.getQuantity()));
		assertThat(mealInventoryGet.getUnit(), is(notNullValue()));
		assertThat(mealInventoryGet.getUnit().getName(),is(mealInventory.getUnit().getName()));
		assertThat(mealInventoryGet.getUnitPrice(),is(mealInventory.getUnitPrice()));
		assertThat(mealInventoryGet.getUnitSalePrice(),is(mealInventory.getUnitSalePrice()));
		assertThat(mealInventoryGet.getMeal(), is(notNullValue()));
		assertThat(mealInventoryGet.getMeal().getId(),is(mealInventory.getMeal().getId()));
		assertThat(mealInventoryGet.getServiceVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getServiceVendor().getMobileNo(),is(mealInventory.getServiceVendor().getMobileNo()));
		assertThat(mealInventoryGet.getServiceVendorUnitSalePrice(),is(mealInventory.getServiceVendorUnitSalePrice()));
		assertThat(mealInventoryGet.getVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getVendor().getMobileNo(),is(mealInventory.getVendor().getMobileNo()));
		
		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("value", "test2");
		List<MealInventory> mealInventories = mealInventoryClient.find(parameters, Collections.emptyList());
		assertThat(mealInventories.size(), is(1));
		mealInventoryGet = mealInventories.get(0);
		assertThat(mealInventoryGet.getCreateDate(),is(mealInventory.getCreateDate()));
		assertThat(mealInventoryGet.getId(),is(mealInventory.getId()));
		assertThat(mealInventoryGet.getNotes(),is(mealInventory.getNotes()));
		assertThat(mealInventoryGet.getProductId(),is(mealInventory.getProductId()));
		assertThat(mealInventoryGet.getQuantity(),is(mealInventory.getQuantity()));
		assertThat(mealInventoryGet.getUnit(), is(notNullValue()));
		assertThat(mealInventoryGet.getUnit().getName(),is(mealInventory.getUnit().getName()));
		assertThat(mealInventoryGet.getUnitPrice(),is(mealInventory.getUnitPrice()));
		assertThat(mealInventoryGet.getUnitSalePrice(),is(mealInventory.getUnitSalePrice()));
		assertThat(mealInventoryGet.getMeal(), is(notNullValue()));
		assertThat(mealInventoryGet.getMeal().getId(),is(mealInventory.getMeal().getId()));
		assertThat(mealInventoryGet.getServiceVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getServiceVendor().getMobileNo(),is(mealInventory.getServiceVendor().getMobileNo()));
		assertThat(mealInventoryGet.getServiceVendorUnitSalePrice(),is(mealInventory.getServiceVendorUnitSalePrice()));
		assertThat(mealInventoryGet.getVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getVendor().getMobileNo(),is(mealInventory.getVendor().getMobileNo()));
		
		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		mealInventories = mealInventoryClient.find(parameters, Collections.emptyList());
		assertThat(mealInventories.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		mealInventories = mealInventoryClient.find(parameters, Arrays.asList("test1","test2"));
		assertThat(mealInventories.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		mealInventories = mealInventoryClient.find(parameters, Arrays.asList("test1"));
		assertThat(mealInventories.isEmpty(), is(true));

		mealInventories = mealInventoryClient.find(Collections.emptyMap(), Arrays.asList("test1","test2"));
		assertThat(mealInventories.isEmpty(), is(true));
		
		mealInventories = mealInventoryClient.find(Collections.emptyMap(), Collections.emptyList());
		assertThat(mealInventories.size(), is(1));
		mealInventoryGet = mealInventories.get(0);
		assertThat(mealInventoryGet.getCreateDate(),is(mealInventory.getCreateDate()));
		assertThat(mealInventoryGet.getId(),is(mealInventory.getId()));
		assertThat(mealInventoryGet.getNotes(),is(mealInventory.getNotes()));
		assertThat(mealInventoryGet.getProductId(),is(mealInventory.getProductId()));
		assertThat(mealInventoryGet.getQuantity(),is(mealInventory.getQuantity()));
		assertThat(mealInventoryGet.getUnit(), is(notNullValue()));
		assertThat(mealInventoryGet.getUnit().getName(),is(mealInventory.getUnit().getName()));
		assertThat(mealInventoryGet.getUnitPrice(),is(mealInventory.getUnitPrice()));
		assertThat(mealInventoryGet.getUnitSalePrice(),is(mealInventory.getUnitSalePrice()));
		assertThat(mealInventoryGet.getMeal(), is(notNullValue()));
		assertThat(mealInventoryGet.getMeal().getId(),is(mealInventory.getMeal().getId()));
		assertThat(mealInventoryGet.getServiceVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getServiceVendor().getMobileNo(),is(mealInventory.getServiceVendor().getMobileNo()));
		assertThat(mealInventoryGet.getServiceVendorUnitSalePrice(),is(mealInventory.getServiceVendorUnitSalePrice()));
		assertThat(mealInventoryGet.getVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getVendor().getMobileNo(),is(mealInventory.getVendor().getMobileNo()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		mealInventories = mealInventoryClient.find(parameters, Collections.emptyList());
		assertThat(mealInventories.size(), is(1));
		mealInventoryGet = mealInventories.get(0);
		assertThat(mealInventoryGet.getCreateDate(),is(mealInventory.getCreateDate()));
		assertThat(mealInventoryGet.getId(),is(mealInventory.getId()));
		assertThat(mealInventoryGet.getNotes(),is(mealInventory.getNotes()));
		assertThat(mealInventoryGet.getProductId(),is(mealInventory.getProductId()));
		assertThat(mealInventoryGet.getQuantity(),is(mealInventory.getQuantity()));
		assertThat(mealInventoryGet.getUnit(), is(notNullValue()));
		assertThat(mealInventoryGet.getUnit().getName(),is(mealInventory.getUnit().getName()));
		assertThat(mealInventoryGet.getUnitPrice(),is(mealInventory.getUnitPrice()));
		assertThat(mealInventoryGet.getUnitSalePrice(),is(mealInventory.getUnitSalePrice()));
		assertThat(mealInventoryGet.getMeal(), is(notNullValue()));
		assertThat(mealInventoryGet.getMeal().getId(),is(mealInventory.getMeal().getId()));
		assertThat(mealInventoryGet.getServiceVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getServiceVendor().getMobileNo(),is(mealInventory.getServiceVendor().getMobileNo()));
		assertThat(mealInventoryGet.getServiceVendorUnitSalePrice(),is(mealInventory.getServiceVendorUnitSalePrice()));
		assertThat(mealInventoryGet.getVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getVendor().getMobileNo(),is(mealInventory.getVendor().getMobileNo()));
		
		mealInventories = mealInventoryClient.find(Collections.emptyMap(), Arrays.asList("test1"));
		assertThat(mealInventories.size(), is(1));
		mealInventoryGet = mealInventories.get(0);
		assertThat(mealInventoryGet.getCreateDate(),is(mealInventory.getCreateDate()));
		assertThat(mealInventoryGet.getId(),is(mealInventory.getId()));
		assertThat(mealInventoryGet.getNotes(),is(mealInventory.getNotes()));
		assertThat(mealInventoryGet.getProductId(),is(mealInventory.getProductId()));
		assertThat(mealInventoryGet.getQuantity(),is(mealInventory.getQuantity()));
		assertThat(mealInventoryGet.getUnit(), is(notNullValue()));
		assertThat(mealInventoryGet.getUnit().getName(),is(mealInventory.getUnit().getName()));
		assertThat(mealInventoryGet.getUnitPrice(),is(mealInventory.getUnitPrice()));
		assertThat(mealInventoryGet.getUnitSalePrice(),is(mealInventory.getUnitSalePrice()));
		assertThat(mealInventoryGet.getMeal(), is(notNullValue()));
		assertThat(mealInventoryGet.getMeal().getId(),is(mealInventory.getMeal().getId()));
		assertThat(mealInventoryGet.getServiceVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getServiceVendor().getMobileNo(),is(mealInventory.getServiceVendor().getMobileNo()));
		assertThat(mealInventoryGet.getServiceVendorUnitSalePrice(),is(mealInventory.getServiceVendorUnitSalePrice()));
		assertThat(mealInventoryGet.getVendor(), is(notNullValue()));
		assertThat(mealInventoryGet.getVendor().getMobileNo(),is(mealInventory.getVendor().getMobileNo()));

		ActionResponse actionResponse = mealInventoryClient.create(mealInventory);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = mealInventoryClient.update(mealInventory, 2L);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = mealInventoryClient.delete(3L);
		assertThat(actionResponse.getHttpCode(), is(204));
	}
	
	@Inject
	private OrderClient orderClient;
	
	@Test
	public void testOrderClient() throws Exception {
		targetClass = Order.class;
		
		assertThat(new TestSystemApp().getRootResource(), is("/order"));
		
		Order orderGet = orderClient.get(1L);
		assertThat(orderGet.getCreateDate(),is(order.getCreateDate()));
		assertThat(orderGet.getId(),is(order.getId()));
		assertThat(orderGet.getInventoryRecord().getId(),is(order.getInventoryRecord().getId()));
		assertThat(orderGet.getQuantity(),is(order.getQuantity()));
		assertThat(orderGet.getRequestor().getMobileNo(),is(order.getRequestor().getMobileNo()));
		assertThat(orderGet.getStatus(),is(order.getStatus()));
		assertThat(orderGet.getUnit(), is(notNullValue()));
		assertThat(orderGet.getUnit().getName(),is(order.getUnit().getName()));

		orderGet = orderClient.get(1L, Arrays.asList("test1"));
		assertThat(orderGet.getCreateDate(),is(order.getCreateDate()));
		assertThat(orderGet.getId(),is(order.getId()));
		assertThat(orderGet.getInventoryRecord().getId(),is(order.getInventoryRecord().getId()));
		assertThat(orderGet.getQuantity(),is(order.getQuantity()));
		assertThat(orderGet.getRequestor().getMobileNo(),is(order.getRequestor().getMobileNo()));
		assertThat(orderGet.getStatus(),is(order.getStatus()));
		assertThat(orderGet.getUnit(), is(notNullValue()));
		assertThat(orderGet.getUnit().getName(),is(order.getUnit().getName()));

		orderGet = orderClient.get(1L, Arrays.asList("test1","test2","test3"));
		assertThat(orderGet.getCreateDate(),is(order.getCreateDate()));
		assertThat(orderGet.getId(),is(order.getId()));
		assertThat(orderGet.getInventoryRecord().getId(),is(order.getInventoryRecord().getId()));
		assertThat(orderGet.getQuantity(),is(order.getQuantity()));
		assertThat(orderGet.getRequestor().getMobileNo(),is(order.getRequestor().getMobileNo()));
		assertThat(orderGet.getStatus(),is(order.getStatus()));
		assertThat(orderGet.getUnit(), is(notNullValue()));
		assertThat(orderGet.getUnit().getName(),is(order.getUnit().getName()));
		
		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("value", "test2");
		List<Order> orders = orderClient.find(parameters, Collections.emptyList());
		assertThat(orders.size(), is(1));
		orderGet = orders.get(0);
		assertThat(orderGet.getCreateDate(),is(order.getCreateDate()));
		assertThat(orderGet.getId(),is(order.getId()));
		assertThat(orderGet.getInventoryRecord().getId(),is(order.getInventoryRecord().getId()));
		assertThat(orderGet.getQuantity(),is(order.getQuantity()));
		assertThat(orderGet.getRequestor().getMobileNo(),is(order.getRequestor().getMobileNo()));
		assertThat(orderGet.getStatus(),is(order.getStatus()));
		assertThat(orderGet.getUnit(), is(notNullValue()));
		assertThat(orderGet.getUnit().getName(),is(order.getUnit().getName()));
		
		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		orders = orderClient.find(parameters, Collections.emptyList());
		assertThat(orders.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		orders = orderClient.find(parameters, Arrays.asList("test1","test2"));
		assertThat(orders.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		orders = orderClient.find(parameters, Arrays.asList("test1"));
		assertThat(orders.isEmpty(), is(true));

		orders = orderClient.find(Collections.emptyMap(), Arrays.asList("test1","test2"));
		assertThat(orders.isEmpty(), is(true));
		
		orders = orderClient.find(Collections.emptyMap(), Collections.emptyList());
		assertThat(orders.size(), is(1));
		orderGet = orders.get(0);
		assertThat(orderGet.getCreateDate(),is(order.getCreateDate()));
		assertThat(orderGet.getId(),is(order.getId()));
		assertThat(orderGet.getInventoryRecord().getId(),is(order.getInventoryRecord().getId()));
		assertThat(orderGet.getQuantity(),is(order.getQuantity()));
		assertThat(orderGet.getRequestor().getMobileNo(),is(order.getRequestor().getMobileNo()));
		assertThat(orderGet.getStatus(),is(order.getStatus()));
		assertThat(orderGet.getUnit(), is(notNullValue()));
		assertThat(orderGet.getUnit().getName(),is(order.getUnit().getName()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		orders = orderClient.find(parameters, Collections.emptyList());
		assertThat(orders.size(), is(1));
		orderGet = orders.get(0);
		assertThat(orderGet.getCreateDate(),is(order.getCreateDate()));
		assertThat(orderGet.getId(),is(order.getId()));
		assertThat(orderGet.getInventoryRecord().getId(),is(order.getInventoryRecord().getId()));
		assertThat(orderGet.getQuantity(),is(order.getQuantity()));
		assertThat(orderGet.getRequestor().getMobileNo(),is(order.getRequestor().getMobileNo()));
		assertThat(orderGet.getStatus(),is(order.getStatus()));
		assertThat(orderGet.getUnit(), is(notNullValue()));
		assertThat(orderGet.getUnit().getName(),is(order.getUnit().getName()));
		
		orders = orderClient.find(Collections.emptyMap(), Arrays.asList("test1"));
		assertThat(orders.size(), is(1));
		orderGet = orders.get(0);
		assertThat(orderGet.getCreateDate(),is(order.getCreateDate()));
		assertThat(orderGet.getId(),is(order.getId()));
		assertThat(orderGet.getInventoryRecord().getId(),is(order.getInventoryRecord().getId()));
		assertThat(orderGet.getQuantity(),is(order.getQuantity()));
		assertThat(orderGet.getRequestor().getMobileNo(),is(order.getRequestor().getMobileNo()));
		assertThat(orderGet.getStatus(),is(order.getStatus()));
		assertThat(orderGet.getUnit(), is(notNullValue()));
		assertThat(orderGet.getUnit().getName(),is(order.getUnit().getName()));
		
		ActionResponse actionResponse = orderClient.create(order);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = orderClient.update(order, 2L);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = orderClient.delete(3L);
		assertThat(actionResponse.getHttpCode(), is(204));
	}
	
	@Inject
	private RegistrationClient registrationClient;
	
	@Test
	public void testRegistrationClient() throws Exception {
		targetClass = Registration.class;
		
		assertThat(new TestSystemApp().getRootResource(), is("/registration"));

		Registration registrationGet = registrationClient.get("1");
		assertThat(registrationGet.getCreateDate(),is(registration.getCreateDate()));
		assertThat(registrationGet.isDeleted(),is(registration.isDeleted()));
		assertThat(registrationGet.getDeviceId(),is(registration.getDeviceId()));
		assertThat(registrationGet.getFullName(),is(registration.getFullName()));
		assertThat(registrationGet.getMobileNo(),is(registration.getMobileNo()));
		assertThat(registrationGet.getPin(),is(registration.getPin()));

		registrationGet = registrationClient.get("1", Arrays.asList("test1"));
		assertThat(registrationGet.getCreateDate(),is(registration.getCreateDate()));
		assertThat(registrationGet.isDeleted(),is(registration.isDeleted()));
		assertThat(registrationGet.getDeviceId(),is(registration.getDeviceId()));
		assertThat(registrationGet.getFullName(),is(registration.getFullName()));
		assertThat(registrationGet.getMobileNo(),is(registration.getMobileNo()));
		assertThat(registrationGet.getPin(),is(registration.getPin()));

		registrationGet = registrationClient.get("1", Arrays.asList("test1","test2","test3"));
		assertThat(registrationGet.getCreateDate(),is(registration.getCreateDate()));
		assertThat(registrationGet.isDeleted(),is(registration.isDeleted()));
		assertThat(registrationGet.getDeviceId(),is(registration.getDeviceId()));
		assertThat(registrationGet.getFullName(),is(registration.getFullName()));
		assertThat(registrationGet.getMobileNo(),is(registration.getMobileNo()));
		assertThat(registrationGet.getPin(),is(registration.getPin()));
		
		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("value", "test2");
		List<Registration> registrations = registrationClient.find(parameters, Collections.emptyList());
		assertThat(registrations.size(), is(1));
		registrationGet = registrations.get(0);
		assertThat(registrationGet.getCreateDate(),is(registration.getCreateDate()));
		assertThat(registrationGet.isDeleted(),is(registration.isDeleted()));
		assertThat(registrationGet.getDeviceId(),is(registration.getDeviceId()));
		assertThat(registrationGet.getFullName(),is(registration.getFullName()));
		assertThat(registrationGet.getMobileNo(),is(registration.getMobileNo()));
		assertThat(registrationGet.getPin(),is(registration.getPin()));
		
		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		registrations = registrationClient.find(parameters, Collections.emptyList());
		assertThat(registrations.isEmpty(), is(true));
		
		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		registrations = registrationClient.find(parameters, Arrays.asList("test1","test2"));
		assertThat(registrations.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		registrations = registrationClient.find(parameters, Arrays.asList("test1"));
		assertThat(registrations.isEmpty(), is(true));

		registrations = registrationClient.find(Collections.emptyMap(), Arrays.asList("test1","test2"));
		assertThat(registrations.isEmpty(), is(true));
		
		registrations = registrationClient.find(Collections.emptyMap(), Collections.emptyList());
		assertThat(registrations.size(), is(1));
		registrationGet = registrations.get(0);
		assertThat(registrationGet.getCreateDate(),is(registration.getCreateDate()));
		assertThat(registrationGet.isDeleted(),is(registration.isDeleted()));
		assertThat(registrationGet.getDeviceId(),is(registration.getDeviceId()));
		assertThat(registrationGet.getFullName(),is(registration.getFullName()));
		assertThat(registrationGet.getMobileNo(),is(registration.getMobileNo()));
		assertThat(registrationGet.getPin(),is(registration.getPin()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		registrations = registrationClient.find(parameters, Collections.emptyList());
		assertThat(registrations.size(), is(1));
		registrationGet = registrations.get(0);
		assertThat(registrationGet.getCreateDate(),is(registration.getCreateDate()));
		assertThat(registrationGet.isDeleted(),is(registration.isDeleted()));
		assertThat(registrationGet.getDeviceId(),is(registration.getDeviceId()));
		assertThat(registrationGet.getFullName(),is(registration.getFullName()));
		assertThat(registrationGet.getMobileNo(),is(registration.getMobileNo()));
		assertThat(registrationGet.getPin(),is(registration.getPin()));
		
		registrations = registrationClient.find(Collections.emptyMap(), Arrays.asList("test1"));
		assertThat(registrations.size(), is(1));
		registrationGet = registrations.get(0);
		assertThat(registrationGet.getCreateDate(),is(registration.getCreateDate()));
		assertThat(registrationGet.isDeleted(),is(registration.isDeleted()));
		assertThat(registrationGet.getDeviceId(),is(registration.getDeviceId()));
		assertThat(registrationGet.getFullName(),is(registration.getFullName()));
		assertThat(registrationGet.getMobileNo(),is(registration.getMobileNo()));
		assertThat(registrationGet.getPin(),is(registration.getPin()));

		ActionResponse actionResponse = registrationClient.create(registration);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = registrationClient.update(registration, "2");
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = registrationClient.delete("3");
		assertThat(actionResponse.getHttpCode(), is(204));
	}
	
	@Inject
	private ServiceVendorRegistrationClient serviceVendorRegistrationClient;
	
	@Test
	public void testServiceVendorRegistrationClient() throws Exception {
		targetClass = ServiceVendorRegistration.class;
		
		assertThat(new TestSystemApp().getRootResource(), is("/service-vendor-registration"));

		ServiceVendorRegistration registration2Get = serviceVendorRegistrationClient.get("1");
		assertThat(registration2Get.getCreateDate(),is(registration2.getCreateDate()));
		assertThat(registration2Get.isDeleted(),is(registration2.isDeleted()));
		assertThat(registration2Get.getDeviceId(),is(registration2.getDeviceId()));
		assertThat(registration2Get.getFullName(),is(registration2.getFullName()));
		assertThat(registration2Get.getMobileNo(),is(registration2.getMobileNo()));
		assertThat(registration2Get.getPin(),is(registration2.getPin()));
		assertThat(registration2Get.isCurrentlyActive(),is(registration2.isCurrentlyActive()));
		assertThat(registration2Get.getServiceLocation(), is(notNullValue()));
		assertThat(registration2Get.getServiceLocation().getId(),
					is(registration2.getServiceLocation().getId()));

		registration2Get = serviceVendorRegistrationClient.get("1", Arrays.asList("test1"));
		assertThat(registration2Get.getCreateDate(),is(registration2.getCreateDate()));
		assertThat(registration2Get.isDeleted(),is(registration2.isDeleted()));
		assertThat(registration2Get.getDeviceId(),is(registration2.getDeviceId()));
		assertThat(registration2Get.getFullName(),is(registration2.getFullName()));
		assertThat(registration2Get.getMobileNo(),is(registration2.getMobileNo()));
		assertThat(registration2Get.getPin(),is(registration2.getPin()));
		assertThat(registration2Get.isCurrentlyActive(),is(registration2.isCurrentlyActive()));
		assertThat(registration2Get.getServiceLocation(), is(notNullValue()));
		assertThat(registration2Get.getServiceLocation().getId(),
					is(registration2.getServiceLocation().getId()));

		registration2Get = serviceVendorRegistrationClient.get("1", Arrays.asList("test1","test2","test3"));
		assertThat(registration2Get.getCreateDate(),is(registration2.getCreateDate()));
		assertThat(registration2Get.isDeleted(),is(registration2.isDeleted()));
		assertThat(registration2Get.getDeviceId(),is(registration2.getDeviceId()));
		assertThat(registration2Get.getFullName(),is(registration2.getFullName()));
		assertThat(registration2Get.getMobileNo(),is(registration2.getMobileNo()));
		assertThat(registration2Get.getPin(),is(registration2.getPin()));
		assertThat(registration2Get.isCurrentlyActive(),is(registration2.isCurrentlyActive()));
		assertThat(registration2Get.getServiceLocation(), is(notNullValue()));
		assertThat(registration2Get.getServiceLocation().getId(),
					is(registration2.getServiceLocation().getId()));
		
		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("value", "test2");
		List<ServiceVendorRegistration> registration2s = serviceVendorRegistrationClient.find(parameters, Collections.emptyList());
		assertThat(registration2s.size(), is(1));
		registration2Get = registration2s.get(0);
		assertThat(registration2Get.getCreateDate(),is(registration2.getCreateDate()));
		assertThat(registration2Get.isDeleted(),is(registration2.isDeleted()));
		assertThat(registration2Get.getDeviceId(),is(registration2.getDeviceId()));
		assertThat(registration2Get.getFullName(),is(registration2.getFullName()));
		assertThat(registration2Get.getMobileNo(),is(registration2.getMobileNo()));
		assertThat(registration2Get.getPin(),is(registration2.getPin()));
		assertThat(registration2Get.isCurrentlyActive(),is(registration2.isCurrentlyActive()));
		assertThat(registration2Get.getServiceLocation(), is(notNullValue()));
		assertThat(registration2Get.getServiceLocation().getId(),
					is(registration2.getServiceLocation().getId()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		registration2s = serviceVendorRegistrationClient.find(parameters, Collections.emptyList());
		assertThat(registration2s.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		registration2s = serviceVendorRegistrationClient.find(parameters, Arrays.asList("test1","test2"));
		assertThat(registration2s.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		registration2s = serviceVendorRegistrationClient.find(parameters, Arrays.asList("test1"));
		assertThat(registration2s.isEmpty(), is(true));

		registration2s = serviceVendorRegistrationClient.find(Collections.emptyMap(), Arrays.asList("test1","test2"));
		assertThat(registration2s.isEmpty(), is(true));
		
		registration2s = serviceVendorRegistrationClient.find(Collections.emptyMap(), Collections.emptyList());
		assertThat(registration2s.size(), is(1));
		registration2Get = registration2s.get(0);
		assertThat(registration2Get.getCreateDate(),is(registration2.getCreateDate()));
		assertThat(registration2Get.isDeleted(),is(registration2.isDeleted()));
		assertThat(registration2Get.getDeviceId(),is(registration2.getDeviceId()));
		assertThat(registration2Get.getFullName(),is(registration2.getFullName()));
		assertThat(registration2Get.getMobileNo(),is(registration2.getMobileNo()));
		assertThat(registration2Get.getPin(),is(registration2.getPin()));
		assertThat(registration2Get.isCurrentlyActive(),is(registration2.isCurrentlyActive()));
		assertThat(registration2Get.getServiceLocation(), is(notNullValue()));
		assertThat(registration2Get.getServiceLocation().getId(),
					is(registration2.getServiceLocation().getId()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		registration2s = serviceVendorRegistrationClient.find(parameters, Collections.emptyList());
		assertThat(registration2s.size(), is(1));
		registration2Get = registration2s.get(0);
		assertThat(registration2Get.getCreateDate(),is(registration2.getCreateDate()));
		assertThat(registration2Get.isDeleted(),is(registration2.isDeleted()));
		assertThat(registration2Get.getDeviceId(),is(registration2.getDeviceId()));
		assertThat(registration2Get.getFullName(),is(registration2.getFullName()));
		assertThat(registration2Get.getMobileNo(),is(registration2.getMobileNo()));
		assertThat(registration2Get.getPin(),is(registration2.getPin()));
		assertThat(registration2Get.isCurrentlyActive(),is(registration2.isCurrentlyActive()));
		assertThat(registration2Get.getServiceLocation(), is(notNullValue()));
		assertThat(registration2Get.getServiceLocation().getId(),
					is(registration2.getServiceLocation().getId()));
		
		registration2s = serviceVendorRegistrationClient.find(Collections.emptyMap(), Arrays.asList("test1"));
		assertThat(registration2s.size(), is(1));
		registration2Get = registration2s.get(0);
		assertThat(registration2Get.getCreateDate(),is(registration2.getCreateDate()));
		assertThat(registration2Get.isDeleted(),is(registration2.isDeleted()));
		assertThat(registration2Get.getDeviceId(),is(registration2.getDeviceId()));
		assertThat(registration2Get.getFullName(),is(registration2.getFullName()));
		assertThat(registration2Get.getMobileNo(),is(registration2.getMobileNo()));
		assertThat(registration2Get.getPin(),is(registration2.getPin()));
		assertThat(registration2Get.isCurrentlyActive(),is(registration2.isCurrentlyActive()));
		assertThat(registration2Get.getServiceLocation(), is(notNullValue()));
		assertThat(registration2Get.getServiceLocation().getId(),
					is(registration2.getServiceLocation().getId()));
		
		ActionResponse actionResponse = serviceVendorRegistrationClient.create(registration2);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = serviceVendorRegistrationClient.update(registration2, "2");
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = serviceVendorRegistrationClient.delete("3");
		assertThat(actionResponse.getHttpCode(), is(204));
	}

	@Inject
	private TicketClient ticketClient;
	
	@Test
	public void testTicketClient() throws Exception {
		targetClass = Ticket.class;
		
		assertThat(new TestSystemApp().getRootResource(), is("/ticket"));

		Ticket ticketGet = ticketClient.get(1L);
		assertThat(ticketGet.isActive(),is(ticket.isActive()));
		assertThat(ticketGet.getCreateDate(),is(ticket.getCreateDate()));
		assertThat(ticketGet.getId(),is(ticket.getId()));
		assertThat(ticketGet.getRegistration(), is(notNullValue()));
		assertThat(ticketGet.getRegistration().getMobileNo(),is(ticket.getRegistration().getMobileNo()));
		assertThat(ticketGet.getTypeCode(),is(ticket.getTypeCode()));
		assertThat(ticketGet.getValue(),is(ticket.getValue()));

		ticketGet = ticketClient.get(1L, Arrays.asList("test1"));
		assertThat(ticketGet.isActive(),is(ticket.isActive()));
		assertThat(ticketGet.getCreateDate(),is(ticket.getCreateDate()));
		assertThat(ticketGet.getId(),is(ticket.getId()));
		assertThat(ticketGet.getRegistration(), is(notNullValue()));
		assertThat(ticketGet.getRegistration().getMobileNo(),is(ticket.getRegistration().getMobileNo()));
		assertThat(ticketGet.getTypeCode(),is(ticket.getTypeCode()));
		assertThat(ticketGet.getValue(),is(ticket.getValue()));

		ticketGet = ticketClient.get(1L, Arrays.asList("test1","test2","test3"));
		assertThat(ticketGet.isActive(),is(ticket.isActive()));
		assertThat(ticketGet.getCreateDate(),is(ticket.getCreateDate()));
		assertThat(ticketGet.getId(),is(ticket.getId()));
		assertThat(ticketGet.getRegistration(), is(notNullValue()));
		assertThat(ticketGet.getRegistration().getMobileNo(),is(ticket.getRegistration().getMobileNo()));
		assertThat(ticketGet.getTypeCode(),is(ticket.getTypeCode()));
		assertThat(ticketGet.getValue(),is(ticket.getValue()));
		
		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("value", "test2");
		List<Ticket> tickets = ticketClient.find(parameters, Collections.emptyList());
		assertThat(tickets.size(), is(1));
		ticketGet = tickets.get(0);
		assertThat(ticketGet.isActive(),is(ticket.isActive()));
		assertThat(ticketGet.getCreateDate(),is(ticket.getCreateDate()));
		assertThat(ticketGet.getId(),is(ticket.getId()));
		assertThat(ticketGet.getRegistration(), is(notNullValue()));
		assertThat(ticketGet.getRegistration().getMobileNo(),is(ticket.getRegistration().getMobileNo()));
		assertThat(ticketGet.getTypeCode(),is(ticket.getTypeCode()));
		assertThat(ticketGet.getValue(),is(ticket.getValue()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		tickets = ticketClient.find(parameters, Collections.emptyList());
		assertThat(tickets.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		tickets = ticketClient.find(parameters, Arrays.asList("test1","test2"));
		assertThat(tickets.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		tickets = ticketClient.find(parameters, Arrays.asList("test1"));
		assertThat(tickets.isEmpty(), is(true));

		tickets = ticketClient.find(Collections.emptyMap(), Arrays.asList("test1","test2"));
		assertThat(tickets.isEmpty(), is(true));
		
		tickets = ticketClient.find(Collections.emptyMap(), Collections.emptyList());
		assertThat(tickets.size(), is(1));
		ticketGet = tickets.get(0);
		assertThat(ticketGet.isActive(),is(ticket.isActive()));
		assertThat(ticketGet.getCreateDate(),is(ticket.getCreateDate()));
		assertThat(ticketGet.getId(),is(ticket.getId()));
		assertThat(ticketGet.getRegistration(), is(notNullValue()));
		assertThat(ticketGet.getRegistration().getMobileNo(),is(ticket.getRegistration().getMobileNo()));
		assertThat(ticketGet.getTypeCode(),is(ticket.getTypeCode()));
		assertThat(ticketGet.getValue(),is(ticket.getValue()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		tickets = ticketClient.find(parameters, Collections.emptyList());
		assertThat(tickets.size(), is(1));
		ticketGet = tickets.get(0);
		assertThat(ticketGet.isActive(),is(ticket.isActive()));
		assertThat(ticketGet.getCreateDate(),is(ticket.getCreateDate()));
		assertThat(ticketGet.getId(),is(ticket.getId()));
		assertThat(ticketGet.getRegistration(), is(notNullValue()));
		assertThat(ticketGet.getRegistration().getMobileNo(),is(ticket.getRegistration().getMobileNo()));
		assertThat(ticketGet.getTypeCode(),is(ticket.getTypeCode()));
		assertThat(ticketGet.getValue(),is(ticket.getValue()));
		
		tickets = ticketClient.find(Collections.emptyMap(), Arrays.asList("test1"));
		assertThat(tickets.size(), is(1));
		ticketGet = tickets.get(0);
		assertThat(ticketGet.isActive(),is(ticket.isActive()));
		assertThat(ticketGet.getCreateDate(),is(ticket.getCreateDate()));
		assertThat(ticketGet.getId(),is(ticket.getId()));
		assertThat(ticketGet.getRegistration(), is(notNullValue()));
		assertThat(ticketGet.getRegistration().getMobileNo(),is(ticket.getRegistration().getMobileNo()));
		assertThat(ticketGet.getTypeCode(),is(ticket.getTypeCode()));
		assertThat(ticketGet.getValue(),is(ticket.getValue()));
		
		ActionResponse actionResponse = ticketClient.create(ticket);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = ticketClient.update(ticket, 2L);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = ticketClient.delete(3L);
		assertThat(actionResponse.getHttpCode(), is(204));
	}
	
	@Inject
	private UnitClient unitClient;
	
	@Test
	public void testUnitClient() throws Exception {
		targetClass = Unit.class;
		
		assertThat(new TestSystemApp().getRootResource(), is("/unit"));

		Unit unitGet = unitClient.get("1");
		assertThat(unitGet.getName(), is(unit.getName()));
		assertThat(unitGet.getSymbol(), is(unit.getSymbol()));
		
		unitGet = unitClient.get("1", Arrays.asList("test1"));
		assertThat(unitGet.getName(), is(unit.getName()));
		assertThat(unitGet.getSymbol(), is(unit.getSymbol()));

		unitGet = unitClient.get("1", Arrays.asList("test1","test2","test3"));
		assertThat(unitGet.getName(), is(unit.getName()));
		assertThat(unitGet.getSymbol(), is(unit.getSymbol()));
		
		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("value", "test2");
		List<Unit> units = unitClient.find(parameters, Collections.emptyList());
		assertThat(units.size(), is(1));
		unitGet = units.get(0);
		assertThat(unitGet.getName(), is(unit.getName()));
		assertThat(unitGet.getSymbol(), is(unit.getSymbol()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		units = unitClient.find(parameters, Collections.emptyList());
		assertThat(units.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		units = unitClient.find(parameters, Arrays.asList("test1","test2"));
		assertThat(units.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		units = unitClient.find(parameters, Arrays.asList("test1"));
		assertThat(units.isEmpty(), is(true));

		units = unitClient.find(Collections.emptyMap(), Arrays.asList("test1","test2"));
		assertThat(units.isEmpty(), is(true));
		
		units = unitClient.find(Collections.emptyMap(), Collections.emptyList());
		assertThat(units.size(), is(1));
		unitGet = units.get(0);
		assertThat(unitGet.getName(), is(unit.getName()));
		assertThat(unitGet.getSymbol(), is(unit.getSymbol()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		units = unitClient.find(parameters, Collections.emptyList());
		assertThat(units.size(), is(1));
		unitGet = units.get(0);
		assertThat(unitGet.getName(), is(unit.getName()));
		assertThat(unitGet.getSymbol(), is(unit.getSymbol()));
		
		units = unitClient.find(Collections.emptyMap(), Arrays.asList("test1"));
		assertThat(units.size(), is(1));
		unitGet = units.get(0);
		assertThat(unitGet.getName(), is(unit.getName()));
		assertThat(unitGet.getSymbol(), is(unit.getSymbol()));
		
		ActionResponse actionResponse = unitClient.create(unit);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = unitClient.update(unit, "2");
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = unitClient.delete("3");
		assertThat(actionResponse.getHttpCode(), is(204));
	}
	
	@Inject
	private VendorRegistrationClient vendorRegistrationClient;
	
	@Test
	public void testVendorRegistrationClient() throws Exception {
		targetClass = VendorRegistration.class;
		
		assertThat(new TestSystemApp().getRootResource(), is("/vendor-registration"));

		VendorRegistration registration3Get = vendorRegistrationClient.get("1");
		assertThat(registration3Get.getCreateDate(),is(registration3.getCreateDate()));
		assertThat(registration3Get.isDeleted(),is(registration3.isDeleted()));
		assertThat(registration3Get.getDeviceId(),is(registration3.getDeviceId()));
		assertThat(registration3Get.getFullName(),is(registration3.getFullName()));
		assertThat(registration3Get.getMobileNo(),is(registration3.getMobileNo()));
		assertThat(registration3Get.getPin(),is(registration3.getPin()));
		assertThat(registration3Get.isCentralKitchenVendor(),is(registration3.isCentralKitchenVendor()));
		assertThat(registration3Get.getVendorLocation(), is(notNullValue()));
		assertThat(registration3Get.getVendorLocation().getId(),
				is(registration3.getVendorLocation().getId()));
		
		registration3Get = vendorRegistrationClient.get("1", Arrays.asList("test1"));
		assertThat(registration3Get.getCreateDate(),is(registration3.getCreateDate()));
		assertThat(registration3Get.isDeleted(),is(registration3.isDeleted()));
		assertThat(registration3Get.getDeviceId(),is(registration3.getDeviceId()));
		assertThat(registration3Get.getFullName(),is(registration3.getFullName()));
		assertThat(registration3Get.getMobileNo(),is(registration3.getMobileNo()));
		assertThat(registration3Get.getPin(),is(registration3.getPin()));
		assertThat(registration3Get.isCentralKitchenVendor(),is(registration3.isCentralKitchenVendor()));
		assertThat(registration3Get.getVendorLocation(), is(notNullValue()));
		assertThat(registration3Get.getVendorLocation().getId(),
				is(registration3.getVendorLocation().getId()));
		
		registration3Get = vendorRegistrationClient.get("1", Arrays.asList("test1","test2","test3"));
		assertThat(registration3Get.getCreateDate(),is(registration3.getCreateDate()));
		assertThat(registration3Get.isDeleted(),is(registration3.isDeleted()));
		assertThat(registration3Get.getDeviceId(),is(registration3.getDeviceId()));
		assertThat(registration3Get.getFullName(),is(registration3.getFullName()));
		assertThat(registration3Get.getMobileNo(),is(registration3.getMobileNo()));
		assertThat(registration3Get.getPin(),is(registration3.getPin()));
		assertThat(registration3Get.isCentralKitchenVendor(),is(registration3.isCentralKitchenVendor()));
		assertThat(registration3Get.getVendorLocation(), is(notNullValue()));
		assertThat(registration3Get.getVendorLocation().getId(),
				is(registration3.getVendorLocation().getId()));

		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("value", "test2");
		List<VendorRegistration> registration3s = vendorRegistrationClient.find(parameters, Collections.emptyList());
		assertThat(registration3s.size(), is(1));
		registration3Get = registration3s.get(0);
		assertThat(registration3Get.getCreateDate(),is(registration3.getCreateDate()));
		assertThat(registration3Get.isDeleted(),is(registration3.isDeleted()));
		assertThat(registration3Get.getDeviceId(),is(registration3.getDeviceId()));
		assertThat(registration3Get.getFullName(),is(registration3.getFullName()));
		assertThat(registration3Get.getMobileNo(),is(registration3.getMobileNo()));
		assertThat(registration3Get.getPin(),is(registration3.getPin()));
		assertThat(registration3Get.isCentralKitchenVendor(),is(registration3.isCentralKitchenVendor()));
		assertThat(registration3Get.getVendorLocation(), is(notNullValue()));
		assertThat(registration3Get.getVendorLocation().getId(),
				is(registration3.getVendorLocation().getId()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		registration3s = vendorRegistrationClient.find(parameters, Collections.emptyList());
		assertThat(registration3s.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		parameters.put("id", "3");
		registration3s = vendorRegistrationClient.find(parameters, Arrays.asList("test1","test2"));
		assertThat(registration3s.isEmpty(), is(true));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		registration3s = vendorRegistrationClient.find(parameters, Arrays.asList("test1"));
		assertThat(registration3s.isEmpty(), is(true));

		registration3s = vendorRegistrationClient.find(Collections.emptyMap(), Arrays.asList("test1","test2"));
		assertThat(registration3s.isEmpty(), is(true));
		
		registration3s = vendorRegistrationClient.find(Collections.emptyMap(), Collections.emptyList());
		assertThat(registration3s.size(), is(1));
		registration3Get = registration3s.get(0);
		assertThat(registration3Get.getCreateDate(),is(registration3.getCreateDate()));
		assertThat(registration3Get.isDeleted(),is(registration3.isDeleted()));
		assertThat(registration3Get.getDeviceId(),is(registration3.getDeviceId()));
		assertThat(registration3Get.getFullName(),is(registration3.getFullName()));
		assertThat(registration3Get.getMobileNo(),is(registration3.getMobileNo()));
		assertThat(registration3Get.getPin(),is(registration3.getPin()));
		assertThat(registration3Get.isCentralKitchenVendor(),is(registration3.isCentralKitchenVendor()));
		assertThat(registration3Get.getVendorLocation(), is(notNullValue()));
		assertThat(registration3Get.getVendorLocation().getId(),
				is(registration3.getVendorLocation().getId()));

		parameters = new HashMap<>();
		parameters.put("value", "test2");
		registration3s = vendorRegistrationClient.find(parameters, Collections.emptyList());
		assertThat(registration3s.size(), is(1));
		registration3Get = registration3s.get(0);
		assertThat(registration3Get.getCreateDate(),is(registration3.getCreateDate()));
		assertThat(registration3Get.isDeleted(),is(registration3.isDeleted()));
		assertThat(registration3Get.getDeviceId(),is(registration3.getDeviceId()));
		assertThat(registration3Get.getFullName(),is(registration3.getFullName()));
		assertThat(registration3Get.getMobileNo(),is(registration3.getMobileNo()));
		assertThat(registration3Get.getPin(),is(registration3.getPin()));
		assertThat(registration3Get.isCentralKitchenVendor(),is(registration3.isCentralKitchenVendor()));
		assertThat(registration3Get.getVendorLocation(), is(notNullValue()));
		assertThat(registration3Get.getVendorLocation().getId(),
				is(registration3.getVendorLocation().getId()));
		
		registration3s = vendorRegistrationClient.find(Collections.emptyMap(), Arrays.asList("test1"));
		assertThat(registration3s.size(), is(1));
		registration3Get = registration3s.get(0);
		assertThat(registration3Get.getCreateDate(),is(registration3.getCreateDate()));
		assertThat(registration3Get.isDeleted(),is(registration3.isDeleted()));
		assertThat(registration3Get.getDeviceId(),is(registration3.getDeviceId()));
		assertThat(registration3Get.getFullName(),is(registration3.getFullName()));
		assertThat(registration3Get.getMobileNo(),is(registration3.getMobileNo()));
		assertThat(registration3Get.getPin(),is(registration3.getPin()));
		assertThat(registration3Get.isCentralKitchenVendor(),is(registration3.isCentralKitchenVendor()));
		assertThat(registration3Get.getVendorLocation(), is(notNullValue()));
		assertThat(registration3Get.getVendorLocation().getId(),
				is(registration3.getVendorLocation().getId()));
		
		ActionResponse actionResponse = vendorRegistrationClient.create(registration3);
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = vendorRegistrationClient.update(registration3, "2");
		assertThat(actionResponse.getHttpCode(), is(204));
		
		actionResponse = vendorRegistrationClient.delete("3");
		assertThat(actionResponse.getHttpCode(), is(204));
	}
	
	class InventoryBomb extends Inventory {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public long getId() {
			throw new UnsupportedOperationException();
		}
	}

	Class<?> targetClass;
	
	class TestSystemApp extends SystemAppClient<Object, Object> {
				
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@SuppressWarnings("unchecked")
		@Override
		protected Class<Object> getTargetClass() {
			return (Class<Object>) targetClass;
		}
		
		public String getRootResource(){
			return rootResource;
		};
		
		@SuppressWarnings("unchecked")
		@Override
		protected Class<Object> getIdClass() {
			return (Class<Object>) targetClass;
		}

		@Override
		protected TypeReference<List<Object>> getListTypeReference() {
			return new TypeReference<List<Object>>() {};
		}
	}
	
	public static class TestSystemErrorApp extends SystemAppClient<String, String> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		protected Class<String> getTargetClass() {
			return String.class;
		}

		@Override
		protected Class<String> getIdClass() {
			return String.class;
		}

		@Override
		protected TypeReference<List<String>> getListTypeReference() {
			return new TypeReference<List<String>>() {};
		}
		
		void configureEncodeError() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
			Class<?> httpClientClass = getClass().getSuperclass().getSuperclass()
			.getSuperclass();
			
			Field charsetField = httpClientClass.getDeclaredField("charset");
			charsetField.setAccessible(true);
			charsetField.set(this, "Uknown-Charset");
		}

	}
}
