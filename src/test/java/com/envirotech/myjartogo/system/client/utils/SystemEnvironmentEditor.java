package com.envirotech.myjartogo.system.client.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class SystemEnvironmentEditor {

	private Map<String, String> exposedEnv, copyOfEnv;
	
	@SuppressWarnings("unchecked")
	private SystemEnvironmentEditor() {
		if(exposedEnv == null) {
			Class<?> unmodifiableMapClass = System.getenv().getClass();
			
			try {
				Field mapField = unmodifiableMapClass.getDeclaredField("m");
				mapField.setAccessible(true);
				
				exposedEnv = (Map<String, String>) mapField.get(System.getenv());
				copyOfEnv = new HashMap<>(exposedEnv);
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException("Unable to get environment variable map field", e);
			}
		}
	}
	
	private static SystemEnvironmentEditor instance;
	
	public static SystemEnvironmentEditor instance() {
		if(instance == null)
			instance = new SystemEnvironmentEditor();
		
		return instance;
	}

	public TemporaryBuilder set(String variable) {
		return new TemporaryBuilder(variable);
	}
	
	public class TemporaryBuilder {
		private String variable;
		private TemporaryBuilder(String variable) {
			this.variable = variable;
		}
		
		public SystemEnvironmentEditor as(String newValue) {
			exposedEnv.put(variable, newValue);
			return SystemEnvironmentEditor.this;
		}
	}
	
	public void reset() {
		exposedEnv.clear();
		exposedEnv.putAll(copyOfEnv);
	}
	
	public void clearAll() {
		exposedEnv.clear();
	}
}
