package com.envirotech.myjartogo.system.client;

import java.util.List;

import javax.inject.Singleton;

import com.envirotech.myjartogo.system.client.model.Location;
import com.fasterxml.jackson.core.type.TypeReference;

@Singleton
public class LocationClient 
	extends SystemAppClient<Location, Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8891315756028502180L;
	private final transient TypeReference<List<Location>> 
		typeReference = new TypeReference<List<Location>>() {};
	
	@Override
	protected Class<Location> getTargetClass() {
		return Location.class;
	}

	@Override
	protected Class<Long> getIdClass() {
		return Long.class;
	}

	@Override
	protected TypeReference<List<Location>> getListTypeReference() {
		return typeReference;
	}

}
