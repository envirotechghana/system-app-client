package com.envirotech.myjartogo.system.client.model;

public enum OrderStatus {
	CREATED,
	REVIEWED,
	DISPATCHED,
	DELIVERED
}
