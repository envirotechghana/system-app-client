package com.envirotech.myjartogo.system.client.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ErrorResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7023212693580323035L;
	private List<String> explanations;

	public List<String> getExplanations() {
		return explanations;
	}

	public void setExplanations(List<String> explanations) {
		this.explanations = explanations;
	}
	
	public ErrorResponse addExplanation(String explanation) {
		if(explanations == null)
			explanations = new ArrayList<>();
		
		explanations.add(explanation);
		return this;
	}
}
