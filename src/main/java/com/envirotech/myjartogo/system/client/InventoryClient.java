package com.envirotech.myjartogo.system.client;

import java.util.List;

import javax.inject.Singleton;

import com.envirotech.myjartogo.system.client.model.Inventory;
import com.fasterxml.jackson.core.type.TypeReference;

@Singleton
public class InventoryClient extends SystemAppClient<Inventory, Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9063854189215019507L;

	private final transient TypeReference<List<Inventory>> typeReference 
		= new TypeReference<List<Inventory>>() {};
	
	@Override
	protected Class<Inventory> getTargetClass() {
		return Inventory.class;
	}

	@Override
	protected Class<Long> getIdClass() {
		return Long.class;
	}

	@Override
	protected TypeReference<List<Inventory>> getListTypeReference() {
		return typeReference;
	}
}
