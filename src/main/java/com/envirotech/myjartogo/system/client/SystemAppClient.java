package com.envirotech.myjartogo.system.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.envirotech.myjartogo.config.Environment;
import com.envirotech.myjartogo.system.authentication.AuthenticationService;
import com.envirotech.myjartogo.system.authentication.model.TokenResponse;
import com.envirotech.myjartogo.system.client.exception.SystemAppError;
import com.envirotech.myjartogo.system.client.model.ActionResponse;
import com.envirotech.myjartogo.system.client.model.ErrorResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quakearts.rest.client.HttpClientBuilder;
import com.quakearts.rest.client.HttpObjectClient;
import com.quakearts.rest.client.HttpResponse;
import com.quakearts.rest.client.HttpVerb;
import com.quakearts.rest.client.exception.HttpClientException;
import com.quakearts.rest.client.exception.HttpClientRuntimeException;

public abstract class SystemAppClient<T,R> extends HttpObjectClient {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1292054789486420393L;

	private static final String APPLICATION_JSON = "application/json";

	@Inject
	private ObjectMapper mapper;
	
	@Inject
	private Environment environment;
	
	@Inject
	private AuthenticationService service;

	protected String rootResource;

	private transient TokenResponse token;
	
	public SystemAppClient() {
		rootResource ="/"+process(getTargetClass());
	}

	@PostConstruct
	protected void configure(){
		new SystemAppClientBuilder(this).configure();
	}
	
	protected class SystemAppClientBuilder extends HttpClientBuilder<SystemAppClient<?, ?>> {

		public SystemAppClientBuilder(SystemAppClient<?, ?> httpClient) {
			this.httpClient = httpClient;
		}

		@Override
		public SystemAppClient<?, ?> thenBuild() {
			return httpClient;
		}

		public void configure()  {
			setURLAs(environment.get("system.app.url", "http://localhost:8083"))
			.setCharacterSetAs(environment.get("system.app.charset", "UTF-8"))
			.setConnectTimeoutAs(environment.getInt("system.app.connect.timeout", 30))
			.setReadTimeoutAs(environment.getInt("system.app.read.timeout", 30))
			.thenBuild();
		}
	}
	
	protected abstract Class<T> getTargetClass();

	private String process(Class<T> targetClass) {
		return targetClass.getSimpleName().chars().mapToObj(c->Character.isUpperCase(c)?
				"-"+((char)Character.toLowerCase(c)): Character.toString((char)c)
		).collect(Collectors.joining()).substring(1);
	}

	public T get(R id) throws IOException, HttpClientException {
		return get(id, Collections.emptyList());
	}

	public T get(R id, List<String> filter) throws IOException, HttpClientException {
		validate(id);
		return executeGet(rootResource+"/{0}{1}", getTargetClass(), generateAuthHeader(), 
				id, filter.isEmpty()?"":"?return="+filter.stream().collect(Collectors.joining(",")));
	}
	
	@SuppressWarnings("unchecked")
	public List<T> find(Map<String, String> parameters, List<String> filter) throws IOException, HttpClientException {
		String filterPrefix = !parameters.isEmpty()?"&":"?";
		return executeGet(rootResource+"{0}{1}", List.class, !parameters.isEmpty()?
				"?"+parameters.entrySet().stream()
				.map(entry->entry.getKey()+"="+encode(entry.getValue()))
				.collect(Collectors.joining("&"))
				:"", 
				!filter.isEmpty()?filterPrefix+"return="+filter.stream().collect(Collectors.joining(","))
				:"");
	}
	
	public ActionResponse create(T value) throws IOException, HttpClientException {
		return execute(HttpVerb.POST, rootResource, value, APPLICATION_JSON, 
				ActionResponse.class, generateAuthHeader());
	}
	
	public ActionResponse update(T value, R id) throws IOException, HttpClientException {
		validate(id);
		return execute(HttpVerb.PUT, rootResource+"/{0}", value, APPLICATION_JSON, 
				ActionResponse.class, generateAuthHeader(), id);
	}
	
	public ActionResponse delete(R id) throws IOException, HttpClientException {
		validate(id);
		return execute(HttpVerb.DELETE, rootResource+"/{0}", null, null, 
				ActionResponse.class, generateAuthHeader(), id);
	}

	protected void validate(R id) {
		if(!getIdClass().isAssignableFrom(id.getClass()))
			throw new HttpClientRuntimeException("Invalid ID type. Must be a "+getIdClass().getSimpleName());
	}

	protected abstract Class<R> getIdClass();

	@Override
	protected String encode(String parameter) {
		try {
			return super.encode(parameter);
		} catch (UnsupportedEncodingException e) {
			throw new HttpClientRuntimeException("Encoding error", e);
		}
	}
	
	private Map<String, List<String>> generateAuthHeader() 
			throws IOException, HttpClientException {
		Map<String, List<String>> authHeader = new HashMap<>();
		authHeader.put("Authorization", Arrays.asList("Bearer "+getToken()));
		return authHeader;
	}

	private String getToken() throws IOException, HttpClientException {
		if(token == null || token.hasExpired()){
			token = service.authenticate(environment.get("alias", "system-app"), 
				environment.get("application", "My-Jartogo-System"), 
				environment.get("system.username", "testuser"), 
				environment.get("system.password", "password1"));
		}
		
		return token.getIdToken();
	}
	
	@Override
	protected String writeValueAsString(Object requestValue) throws HttpClientException {
		try {
			return mapper.writeValueAsString(requestValue);
		} catch (JsonProcessingException e) {
			throw new HttpClientException("Unable to marshal request object", e);
		}
	}

	@Override
	protected HttpClientException nonSuccessResponseUsing(HttpResponse httpResponse) {
		try {
			return new SystemAppError(mapper.readValue(httpResponse.getOutputBytes(), ErrorResponse.class),
					httpResponse.getHttpCode());
		} catch (IOException e) {
			return new SystemAppError(new ErrorResponse().addExplanation(httpResponse.getOutput()),
					httpResponse.getHttpCode());
		}
	}

	@Override
	protected <U> Converter<U> createConverter(Class<U> targetClass) {
		return httpResponse->{
			try {
				if(targetClass == ActionResponse.class) {
					return targetClass.cast(new ActionResponse().withHttpCodeAs(httpResponse.getHttpCode()));
				} else if(targetClass == List.class) {
					return mapper.readValue(httpResponse.getOutputBytes(), getListTypeReference());
				} else {
					return mapper.readValue(httpResponse.getOutputBytes(), targetClass);
				}
			} catch (IOException e) {
				throw new SystemAppError(new ErrorResponse().addExplanation(httpResponse.getOutput()),
						httpResponse.getHttpCode());
			}
		};
	}

	protected abstract TypeReference<List<T>> getListTypeReference();

}
