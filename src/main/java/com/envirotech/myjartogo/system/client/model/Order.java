package com.envirotech.myjartogo.system.client.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"id",
	"inventoryRecord",
	"quantity",
	"unit",
	"requestor",
	"status"
})
public class Order implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7384375624880076843L;
	private long id;
	private Inventory inventoryRecord;
	private BigDecimal quantity;
	private Unit unit;
	private Registration requestor;
	private OrderStatus status;	
	private LocalDateTime createDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Inventory getInventoryRecord() {
		return inventoryRecord;
	}

	public void setInventoryRecord(Inventory inventoryRecord) {
		this.inventoryRecord = inventoryRecord;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public Registration getRequestor() {
		return requestor;
	}

	public void setRequestor(Registration requestor) {
		this.requestor = requestor;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

}
