package com.envirotech.myjartogo.system.client.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"id",
	"productId",
	"notes",
	"quantity",
	"unit",
	"unitPrice",
	"unitSalePrice",
	"createDate",
	"meal",
	"vendor",
	"serviceVendor",
	"serviceVendorUnitSalePrice"
})
public class MealInventory extends Inventory {
	/**
	 * 
	 */
	private static final long serialVersionUID = -387826020603268247L;
	private Meal meal;
	private VendorRegistration vendor;
	private ServiceVendorRegistration serviceVendor;
	private BigDecimal serviceVendorUnitSalePrice;

	public VendorRegistration getVendor() {
		return vendor;
	}

	public void setVendor(VendorRegistration vendor) {
		this.vendor = vendor;
	}

	public ServiceVendorRegistration getServiceVendor() {
		return serviceVendor;
	}

	public void setServiceVendor(ServiceVendorRegistration serviceVendor) {
		this.serviceVendor = serviceVendor;
	}

	public BigDecimal getServiceVendorUnitSalePrice() {
		return serviceVendorUnitSalePrice;
	}

	public void setServiceVendorUnitSalePrice(BigDecimal serviceVendorUnitSalePrice) {
		this.serviceVendorUnitSalePrice = serviceVendorUnitSalePrice;
	}

	public Meal getMeal() {
		return meal;
	}

	public void setMeal(Meal meal) {
		this.meal = meal;
	}

}
