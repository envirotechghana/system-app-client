package com.envirotech.myjartogo.system.client;

import java.util.List;

import javax.inject.Singleton;

import com.envirotech.myjartogo.system.client.model.MealInventory;
import com.fasterxml.jackson.core.type.TypeReference;

@Singleton
public class MealInventoryClient 
	extends SystemAppClient<MealInventory,Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4714710260260918485L;
	private final transient TypeReference<List<MealInventory>> 
		typeReference = new TypeReference<List<MealInventory>>() {};

	@Override
	protected Class<MealInventory> getTargetClass() {
		return MealInventory.class;
	}

	@Override
	protected Class<Long> getIdClass() {
		return Long.class;
	}

	@Override
	protected TypeReference<List<MealInventory>> getListTypeReference() {
		return typeReference;
	}

}
