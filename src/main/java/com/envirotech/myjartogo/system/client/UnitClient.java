package com.envirotech.myjartogo.system.client;

import java.util.List;

import javax.inject.Singleton;

import com.envirotech.myjartogo.system.client.model.Unit;
import com.fasterxml.jackson.core.type.TypeReference;

@Singleton
public class UnitClient extends SystemAppClient<Unit, String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1901769295796954771L;
	private final transient TypeReference<List<Unit>> 
		typeReference = new TypeReference<List<Unit>>() {};

	@Override
	protected Class<Unit> getTargetClass() {
		return Unit.class;
	}

	@Override
	protected Class<String> getIdClass() {
		return String.class;
	}

	@Override
	protected TypeReference<List<Unit>> getListTypeReference() {
		return typeReference;
	}

}
