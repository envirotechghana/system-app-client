package com.envirotech.myjartogo.system.client;

import java.util.List;

import javax.inject.Singleton;

import com.envirotech.myjartogo.system.client.model.Ticket;
import com.fasterxml.jackson.core.type.TypeReference;

@Singleton
public class TicketClient extends SystemAppClient<Ticket, Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7702098272798066245L;
	private final transient TypeReference<List<Ticket>> 
		typeReference = new TypeReference<List<Ticket>>() {};

	@Override
	protected Class<Ticket> getTargetClass() {
		return Ticket.class;
	}

	@Override
	protected Class<Long> getIdClass() {
		return Long.class;
	}

	@Override
	protected TypeReference<List<Ticket>> getListTypeReference() {
		return typeReference;
	}
	
}
