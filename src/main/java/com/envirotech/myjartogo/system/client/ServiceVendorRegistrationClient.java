package com.envirotech.myjartogo.system.client;

import java.util.List;

import javax.inject.Singleton;

import com.envirotech.myjartogo.system.client.model.ServiceVendorRegistration;
import com.fasterxml.jackson.core.type.TypeReference;

@Singleton
public class ServiceVendorRegistrationClient 
	extends SystemAppClient<ServiceVendorRegistration, String> {

	private final transient TypeReference<List<ServiceVendorRegistration>> 
		typeReference = new TypeReference<List<ServiceVendorRegistration>>() {};
	/**
	 * 
	 */
	private static final long serialVersionUID = -1988406210550710126L;

	@Override
	protected Class<ServiceVendorRegistration> getTargetClass() {
		return ServiceVendorRegistration.class;
	}

	@Override
	protected Class<String> getIdClass() {
		return String.class;
	}

	@Override
	protected TypeReference<List<ServiceVendorRegistration>> getListTypeReference() {
		return typeReference;
	}

}
