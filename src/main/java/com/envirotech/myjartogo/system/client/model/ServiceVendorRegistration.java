package com.envirotech.myjartogo.system.client.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"mobileNo",
	"fullName",
	"deviceId",
	"pin",
	"createDate",
	"serviceLocation",
	"currentlyActive",
	"deleted"
})
public class ServiceVendorRegistration extends Registration {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7017298428391305739L;
	private Location serviceLocation;
	private Boolean currentlyActive;

	public Location getServiceLocation() {
		return serviceLocation;
	}

	public void setServiceLocation(Location location) {
		this.serviceLocation = location;
	}

	public Boolean isCurrentlyActive() {
		return currentlyActive;
	}

	public void setCurrentlyActive(Boolean currentlyActive) {
		this.currentlyActive = currentlyActive;
	}

}
