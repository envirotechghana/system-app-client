package com.envirotech.myjartogo.system.client;

import java.util.List;

import javax.inject.Singleton;

import com.envirotech.myjartogo.system.client.model.VendorRegistration;
import com.fasterxml.jackson.core.type.TypeReference;

@Singleton
public class VendorRegistrationClient extends SystemAppClient<VendorRegistration, String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7061582849300366965L;
	private final transient TypeReference<List<VendorRegistration>> 
		typeReference = new TypeReference<List<VendorRegistration>>() {};

	@Override
	protected Class<VendorRegistration> getTargetClass() {
		return VendorRegistration.class;
	}

	@Override
	protected Class<String> getIdClass() {
		return String.class;
	}

	@Override
	protected TypeReference<List<VendorRegistration>> getListTypeReference() {
		return typeReference;
	}

}
