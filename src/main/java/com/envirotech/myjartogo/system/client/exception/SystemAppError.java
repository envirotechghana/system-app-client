package com.envirotech.myjartogo.system.client.exception;

import com.envirotech.myjartogo.system.client.model.ErrorResponse;
import com.quakearts.rest.client.exception.HttpClientException;

public class SystemAppError extends HttpClientException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -440017862265679020L;
	private final int httpCode;
	private final ErrorResponse errorResponse;

	public SystemAppError(ErrorResponse errorResponse, int httpCode) {
		super("System App Error");
		this.httpCode = httpCode;
		this.errorResponse = errorResponse;
	}
	
	public int getHttpCode() {
		return httpCode;
	}
	
	public ErrorResponse getErrorResponse() {
		return errorResponse;
	}
}
