package com.envirotech.myjartogo.system.client.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"id",
	"productId",
	"notes",
	"quantity",
	"unit",
	"unitPrice",
	"unitSalePrice",
	"createDate"
})
public class Inventory implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6866046482535780612L;
	private long id;
	private String productId;
	private String notes;
	private BigDecimal quantity;
	private Unit unit;
	private BigDecimal unitPrice;
	private BigDecimal unitSalePrice;
	private LocalDateTime createDate;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getUnitSalePrice() {
		return unitSalePrice;
	}

	public void setUnitSalePrice(BigDecimal unitSalePrice) {
		this.unitSalePrice = unitSalePrice;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}
 
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

}
