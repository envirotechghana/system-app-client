package com.envirotech.myjartogo.system.client.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"mobileNo",
	"fullName",
	"deviceId",
	"pin",
	"createDate",
	"serviceLocation",
	"currentlyActive",
	"vendorLocation",
	"centralKitchenVendor",
	"deleted"
})
public class VendorRegistration extends Registration {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6691381984747137209L;
	private Location vendorLocation;
	private boolean centralKitchenVendor;

	public boolean isCentralKitchenVendor() {
		return centralKitchenVendor;
	}

	public void setCentralKitchenVendor(boolean centralKitchenVendor) {
		this.centralKitchenVendor = centralKitchenVendor;
	}

	public Location getVendorLocation() {
		return vendorLocation;
	}

	public void setVendorLocation(Location vendorLocation) {
		this.vendorLocation = vendorLocation;
	}
}
