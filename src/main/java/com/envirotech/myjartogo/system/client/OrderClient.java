package com.envirotech.myjartogo.system.client;

import java.util.List;

import javax.inject.Singleton;

import com.envirotech.myjartogo.system.client.model.Order;
import com.fasterxml.jackson.core.type.TypeReference;

@Singleton
public class OrderClient 
	extends SystemAppClient<Order, Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2584195772031079767L;
	private final transient TypeReference<List<Order>> 
		typeReference = new TypeReference<List<Order>>() {};

	@Override
	protected Class<Order> getTargetClass() {
		return Order.class;
	}

	@Override
	protected Class<Long> getIdClass() {
		return Long.class;
	}

	@Override
	protected TypeReference<List<Order>> getListTypeReference() {
		return typeReference;
	}

}
