package com.envirotech.myjartogo.system.client;

import java.util.List;

import javax.inject.Singleton;

import com.envirotech.myjartogo.system.client.model.Meal;
import com.fasterxml.jackson.core.type.TypeReference;

@Singleton
public class MealClient 
	extends SystemAppClient<Meal, Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1744928014708909479L;
	private final transient TypeReference<List<Meal>> 
		typeReference = new TypeReference<List<Meal>>() {};

	@Override
	protected Class<Meal> getTargetClass() {
		return Meal.class;
	}

	@Override
	protected Class<Long> getIdClass() {
		return Long.class;
	}

	@Override
	protected TypeReference<List<Meal>> getListTypeReference() {
		return typeReference;
	}

	
}
