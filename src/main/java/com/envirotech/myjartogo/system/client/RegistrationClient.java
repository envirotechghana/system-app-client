package com.envirotech.myjartogo.system.client;

import java.util.List;

import javax.inject.Singleton;

import com.envirotech.myjartogo.system.client.model.Registration;
import com.fasterxml.jackson.core.type.TypeReference;

@Singleton
public class RegistrationClient 
	extends SystemAppClient<Registration, String> {

	private final transient TypeReference<List<Registration>> 
		typeReference = new TypeReference<List<Registration>>() {};
	/**
	 * 
	 */
	private static final long serialVersionUID = -3714270472721249772L;

	@Override
	protected Class<Registration> getTargetClass() {
		return Registration.class;
	}

	@Override
	protected Class<String> getIdClass() {
		return String.class;
	}

	@Override
	protected TypeReference<List<Registration>> getListTypeReference() {
		return typeReference;
	}
	
}
