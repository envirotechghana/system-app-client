package com.envirotech.myjartogo.system.client.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"name",
	"symbol"
})
public class Unit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7023851198549259447L;
	private String name;
	private String symbol;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
}
