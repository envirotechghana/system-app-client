package com.envirotech.myjartogo.system.client.model;

public class ActionResponse {
	private int httpCode;
	
	public int getHttpCode() {
		return httpCode;
	}
	
	public ActionResponse withHttpCodeAs(int httpCode) {
		this.httpCode = httpCode;
		return this;
	}
}
